﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;
using ToDoList.Repositories;
using ToDoList.ViewModel;

namespace ToDoList.Services
{
    public class EmployeeService
    {
        private int pageSize = 3;
        private EmployeeRepository employeeRepository;

        public EmployeeService()
        {
            employeeRepository = new EmployeeRepository();    
        }

        public string GetMd5hash(MD5 md5hash, string input)
        {
            byte[] data = md5hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }

        public bool VerifyMd5Hash(MD5 md5hash, string input, string hash)
        {
            var hashOfInput = GetMd5hash(md5hash, input);
            if (String.Compare(hash, hashOfInput) == 0)
            {
                return true;
            }
            return false;
        }

        public Employee Login(string username, string password)
        {
            MD5 md5 = MD5.Create();
            var hashPassword = GetMd5hash(md5, password);

            Expression<Func<Employee, bool>> predicate = m => m.Email.Equals(username) && m.C_Password.ToLower().Equals(hashPassword.ToLower());
            Employee employee = employeeRepository.Find(predicate).FirstOrDefault();

            return employee == null ? null : employee;
        }

        public EmployeeIndexVM getAllEmployee(string sortOrder, string filter, int pageIndex)
        {
            Expression<Func<Employee, bool>> predicate = m => m.RoleId == 2;
           
            if(!String.IsNullOrEmpty(filter))
            {
                predicate = m => m.Name.ToLower().Contains(filter.ToLower()) && m.RoleId == 2;    
            }

            var employees = employeeRepository.Find(predicate);

            switch (sortOrder)
            {
                case "Name_desc":
                    employees = employees.OrderByDescending(e => e.Name);
                    break;
                //case "Name_desc":
                //    employees = employees.OrderByDescending(e => e.Name);
                //    break;
                case "Birthdate":
                    employees = employees.OrderBy(e => e.BirthDate);
                    break;
                case "Birthdate_desc":
                    employees = employees.OrderByDescending(e => e.BirthDate);
                    break;
                default:
                    employees = employees.OrderBy(e => e.Name);
                    break;
            }

            return new EmployeeIndexVM
            {
                Employees = PaginatedList<Employee>.Create(employees.ToList(), pageIndex, pageSize)
            };

        }

        public EmployeeIndexVM getAllAdmin(string sortOrder, string filter, int pageIndex)
        {
            Expression<Func<Employee, bool>> predicate = m => m.RoleId == 1;

            if (!String.IsNullOrEmpty(filter))
            {
                predicate = m => m.Name.ToLower().Contains(filter.ToLower()) && m.RoleId == 1;
            }

            var employees = employeeRepository.Find(predicate);

            switch (sortOrder)
            {
                case "Name_desc":
                    employees = employees.OrderByDescending(e => e.Name);
                    break;
                case "Birthdate":
                    employees = employees.OrderBy(e => e.BirthDate);
                    break;
                case "Birthdate_desc":
                    employees = employees.OrderByDescending(e => e.BirthDate);
                    break;
                default:
                    employees = employees.OrderBy(e => e.Name);
                    break;
            }
            return new EmployeeIndexVM
            {
                Employees = PaginatedList<Employee>.Create(employees.ToList(), pageIndex, pageSize)
            };
        }
        public void lockAndUnlockUser(int userId, bool status)
        {
            if (userId < 0 ) return;
            var employee = employeeRepository.GetById(userId);

            if (employee == null) return;
            employee.IsActive = status;
            employeeRepository.complete();
        } 

        public void createEmployee(Employee employee)
        {
            employee.C_Password = GetMd5hash(MD5.Create(), "123456");
            employee.IsActive = true;
            employeeRepository.Add(employee);
        }

        public bool isExistEmail(string email, int? id)
        {
            var employees = employeeRepository.GetAll();
            foreach(Employee employee in employees) {
                if (id != null) {
                    if (employee.Email.Equals(email) && id == employee.Id)
                    {
                        return false;
                    } else if(employee.Email.Equals(email) && id != employee.Id)
                    {
                        return true;
                    }
                } else
                {
                    if (employee.Email.Equals(email))
                    {
                        return true;
                    }
                }
               
            }
            return false;
        }

        public Employee getById(int? id)
        {
            if (id == null) return new Employee();
            return employeeRepository.GetById(id.GetValueOrDefault());
        }

        public void edit(Employee employee)
        {
            Employee e = getById(employee.Id);
            if (e == null) return;
            e.Name = employee.Name;
            e.BirthDate = employee.BirthDate;
            e.Email = employee.Email;
            e.Gender = employee.Gender;
            e.RoleId = employee.RoleId;
            employeeRepository.complete();
        }
    }
}