﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;
using ToDoList.Repositories;
using ToDoList.ViewModel;

namespace ToDoList.Services
{
    public class TaskService
    {
        int pageSize = 3;

        private TaskRepository taskRepository;
        private AssignmentRepository assignmentRepository;
        private CommentRepository commentRepository;
        private TaskLogRepsitory taskLogRepository;
        private TaskStatusRepository taskStatusRepository;
        private EmployeeRepository employeeRepository;
        private FileManagerRepository fileManagerRepository;

        public TaskService()
        {
            taskRepository = new TaskRepository();
            assignmentRepository = new AssignmentRepository();
            commentRepository = new CommentRepository();
            taskLogRepository = new TaskLogRepsitory();
            taskStatusRepository = new TaskStatusRepository();
            employeeRepository = new EmployeeRepository();
            fileManagerRepository = new FileManagerRepository();
        }

        public void autoUpdateTask()
        {
            //auto update task
            var tasks = taskRepository.GetAll();
            autoUpdateTaskStatusWhenExpired(tasks.ToList());
        }

        public TaskIndexVM getAllTaskOfUser(string filter, int pageIndex, int userId)
        {
            if (!isExistsTaskOfUser(userId)) return new TaskIndexVM
            {
                Tasks = PaginatedList<Task>.Create(new List<Task>(), pageIndex, pageSize)
            };

            var tasks = taskRepository.getTaskOfUser(userId);

            if (!String.IsNullOrEmpty(filter))
            {
               tasks = filterTaskByTitle(tasks, filter);
            }

            return new TaskIndexVM
            {
                Tasks = PaginatedList<Task>.Create(tasks.OrderByDescending(m => m.StartDate).ToList(), pageIndex, pageSize)
            };

        }

        public TaskIndexVM getAllTask(String filter, int pageIndex)
        {
            Expression<Func<Task, bool>> predicate = m => true;

            if (!String.IsNullOrEmpty(filter))
            {
                predicate = m => m.Title.ToLower().Contains(filter.ToLower());
            }
            var tasks = taskRepository.Find(predicate);

            return new TaskIndexVM
            {
                Tasks = PaginatedList<Task>.Create(tasks.OrderByDescending(m => m.StartDate).ToList(), pageIndex, pageSize)
            };
        }

        public TaskIndexVM getAllPublicTask(string searchString, int pageIndex)
        {
            Expression<Func<Task, bool>> predicate = m => m.Scope == true;

            if (!String.IsNullOrEmpty(searchString))
            {
                predicate = m => m.Title.ToLower().Contains(searchString.ToLower()) && m.Scope == true;
            }
            var tasks = taskRepository.Find(predicate);

            return new TaskIndexVM
            {
                Tasks = PaginatedList<Task>.Create(tasks.OrderByDescending(m => m.StartDate).ToList(), pageIndex, pageSize)
            };
        }

        public bool isExistsTaskOfUser(int userId)
        {
            var assignments = assignmentRepository.GetAll();
            foreach (Assignment assignment in assignments)
            {
                if (assignment.EmployeeId == userId)
                {
                    return true;
                }
            }
            return false;
        }

        public bool isUserAssignForTask(int userId, int taskId)
        {
            var assignments = assignmentRepository.GetAll();
            foreach (Assignment assignment in assignments)
            {
                if (assignment.EmployeeId == userId && assignment.TaskId == taskId)
                {
                    return true;
                }
            }
            return false;
        }

        public IEnumerable<Task> filterTaskByTitle(IEnumerable<Task> tasks, String title)
        {
            List<Task> results = new List<Task>();
            foreach (Task task in tasks)
            {
                if (task.Title.Contains(title))
                {
                    results.Add(task);
                }
            }
            return results.ToList();
        }

        public void updateScopeOfTask(int taskId, bool scope)
        {
            //update status of task
            Task task = taskRepository.GetById(taskId);
            task.Scope = scope;
            taskRepository.complete();
        }

        // everytime when load task, run this function to auto update task status when it expired
        private void autoUpdateTaskStatusWhenExpired(List<Task> tasks)
        {
            foreach(Task task in tasks )
            {
                bool islater = DateTime.Compare(task.EndDate, DateTime.Now) < 0 && DateTime.Compare(task.StartDate, DateTime.Now) < 0;
                if (islater && task.StatusId == 1 )
                {
                    task.StatusId = 3;
                    taskRepository.complete();
                }
            }
        }

        private bool isTaskScopeChange(int taskId, bool scope)
        {
            if (scope == taskRepository.GetById(taskId).Scope)
                return false;
            return true;
        }

        public string writeTaskLogWhenEditTaskScope(int taskId, int role, int userId, bool scope)
        {
            if(isTaskScopeChange(taskId,scope))
            {
                var taskLogContent = "";
                TaskLog taskLog = new TaskLog();

                if (role == 1) // nói chứ cái này đức ghi á nha riêng phần write scope là đức ghi vc đức đay 
                {
                    taskLogContent = "+ Quản trị viên đã cập nhật phạm vi công việc thành " + Convert.ToString(scope ? "public" : "private") + "\n";
                }
                else if (role == 2)
                {
                    taskLogContent = "+ Nhân viên đã cập nhật phạm vi công việc thành " + Convert.ToString(scope ? "public" : "private") + "\n";
                }
                else
                {
                    taskLogContent = "+ Ban điều hành đã cập nhật phạm vi công việc thành " + Convert.ToString(scope ? "public" : "private") + "\n";
                }

                return taskLogContent;
            }
            return null;
        }

        public void writeTaskLogWhenTaskScopeChange(int taskId, string role, string userId, bool scope)
        {
            var taskLogContent = "Đã cập nhật trạng thái công việc thành " + Convert.ToString(scope ? "public" : "private");
            TaskLog taskLog = new TaskLog();

            taskLog.TaskId = taskId;
            taskLog.EmployeeId = Int32.Parse(userId);
            taskLog.Content = taskLogContent;
            taskLog.C_TimeStamp = DateTime.Now;
            taskLogRepository.Add(taskLog);
            taskLogRepository.complete();
        }

        /*HANDLE IN PAGE TASK DETAIL*/
        public DetailTaskVM loadTaskDetail(int? taskId)
        {
            // get task detail
            Task task = taskRepository.GetById(taskId.GetValueOrDefault());
            if (task == null) return null;
            return new DetailTaskVM
            {
                task = task,
                Comments = taskRepository.loadCommentOfTask(taskId.GetValueOrDefault()),
                TaskLogs = taskRepository.loadTaskLogOfTask(taskId.GetValueOrDefault()),
                EmployeeAssign = taskRepository.getEmployeeAssignToTask(taskId.GetValueOrDefault()),
                Files = taskRepository.loadFileOfTask(taskId.GetValueOrDefault())
            };
        }


        public void addComment(Comment comment)
        {
            if (comment == null) return;
            commentRepository.Add(comment);
            commentRepository.complete();
        }

        public void AddAssignment(Assignment a)
        {
            assignmentRepository.Add(a);
        }

        public List<Employee> GetEmployeeByTaskId(int? id)
        {
            return assignmentRepository.GetEmployeeByTaskId(id.GetValueOrDefault());
        }

        public TaskCreateVM getTaskCreateVM(int? id)
        {
            TaskCreateVM vm = new TaskCreateVM();

            Task t = taskRepository.GetById(id.GetValueOrDefault());

            vm.EndDate = t.EndDate;
            vm.StartDate = t.StartDate;
            vm.Scope = t.Scope;
            vm.StatusId = t.StatusId;
            vm.Title = t.Title;

            return vm;
        }

        private bool isTaskTitleChange(int taskId, string newTitle)
        {
            string oldTitle = taskRepository.GetById(taskId).Title;
            if (oldTitle.Equals(newTitle))
                return false;
            return true;
        }

        public string writeTaskLogWhenTitleChange(int taskId,int role,int userId, string title)
        {
            if(isTaskTitleChange(taskId,title))
            {
                string oldTitle = taskRepository.GetById(taskId).Title;
                if (role == 1)
                {
                    string result = "+ Quản trị viên sửa tiêu đề công việc từ " + "'" + oldTitle  + "' thành '" + title + "'" + '\n' ;
                    return result;
                }
                else
                {
                    string result = "+ Nhân viên sửa tiêu đề công việc từ " + "'" + oldTitle + "' thành '" + title + "'\n";
                    return result;
                }

            }
            return null;
        }

        private bool isTaskStartDateChange(int taskId, DateTime newDate)
        {
            DateTime oldDate = taskRepository.GetById(taskId).StartDate;
            if (DateTime.Compare(oldDate,newDate) == 0)
                return false;
            return true;
        }

        public string writeTaskLogWhenStartDateChange(int taskId, int role, int userId, DateTime date)
        {
            if (isTaskStartDateChange(taskId, date))
            {
                DateTime oldDate = taskRepository.GetById(taskId).StartDate;
                if (role == 1)
                {
                    string result = "+ Quản trị viên sửa ngày bắt đầu công việc từ " + "'" + oldDate  + "' thành '" + date + "'\n";
                    return result;
                }
                else
                {
                    string result = "+ Nhân viên sửa ngày bắt đầu công việc từ " + "'" + oldDate + "' thành '" + date + "'\n";
                    return result;
                }

            }
            return null;
        }

        private bool isTaskEndDateChange(int taskId, DateTime newDate)
        {
            DateTime oldDate = taskRepository.GetById(taskId).EndDate;
            if (DateTime.Compare(oldDate,newDate) == 0)
                return false;
            return true;
        }

        public string writeTaskLogWhenEndDateChange(int taskId, int role, int userId, DateTime date)
        {
            if (isTaskEndDateChange(taskId, date))
            {
                DateTime oldDate = taskRepository.GetById(taskId).EndDate;
                if (role == 1)
                {
                    string result = "+ Quản trị viên sửa ngày kết thúc công việc từ " + "'" + oldDate + "' thành '" + date + "'\n";
                    return result;
                }
                else
                {
                    string result = "+ Nhân viên sửa ngày kết thúc công việc từ " + "'" + oldDate + "' thành '" + date + "'\n";
                    return result;
                }

            }
            return null;
        }

        private bool isTaskStatusChange(int taskId, int newStatusId)
        {
            int oldStatusId = taskRepository.GetById(taskId).StatusId;
            if (oldStatusId == newStatusId)
                return false;
            return true;
        }

        public string writeTaskLogWhenStatusChange(int taskId, int role, int userId, int statusId)
        {
            if (isTaskStatusChange(taskId, statusId))
            {
                string oldStatus = taskStatusRepository.GetById(taskRepository.GetById(taskId).StatusId).StatusName;
                string newStatus = taskStatusRepository.GetById(statusId).StatusName;
                if (role == 1)
                {
                    string result = "+ Quản trị viên sửa trạng thái công việc từ " + "'" + oldStatus + "' thành '" + newStatus + "'\n";
                    return result;
                }
                else
                {
                    string result = "+ Nhân viên sửa trạng thái công việc từ " + "'" + oldStatus + "' thành '" + newStatus + "'\n";
                    return result;
                }

            }
            return null;
        }

        private bool isTaskAssignChange(int taskId, List<int> newAssign, List<int> oldAssign)
        {
            if(newAssign.Count != oldAssign.Count )
            {
                return true;
            }
            else
            {
                foreach (int old in oldAssign)
                {
                    //note search contains
                    bool flag = true;
                    foreach (int newE in newAssign)
                    {
                        if (old == newE)
                        {
                            flag = false;
                            break;
                        }

                    }
                    if (flag == true)
                        return true;
                }
                return false;
            }
            
        }

        public string getStringEmployeeAssigned(List<int> assign)
        {
            string result = "";
            if(assign.Count != 0 )
            {
                foreach (int item in assign)
                {
                    result += employeeRepository.GetById(item).Name + ", ";
                }
                result = result.Remove(result.Length - 2);
            }
            return result;
        }

        public string writeTaskLogAssignChange(int taskId, int role, int userId, List<int> assign)
        {
            List<int> oldAssign = ListEmployeeIdIsAssign(GetEmployeeByTaskId(taskId));
            if (isTaskAssignChange(taskId, assign,oldAssign))
            {
                if (role == 1)
                {
                    string result = "+ Quản trị viên sửa phân công việc từ " + "'" + getStringEmployeeAssigned(oldAssign) + "' thành '" + getStringEmployeeAssigned(assign) + "'\n";
                    return result;
                }
                else
                {
                    string result = "+ Nhân viên sửa phân công việc từ " + "'" + getStringEmployeeAssigned(oldAssign) + "' thành '" + getStringEmployeeAssigned(assign) + "'\n";
                    return result;
                }

            }
            return null;
        }

        public List<int> ListEmployeeIdIsAssign(List<Employee> list)
        {
            List<int> result = new List<int>();
            foreach (Employee e in list)
            {
                result.Add(e.Id);
            }
            return result;
        }

        public void AddTaskLog(TaskLog t)
        {
            taskLogRepository.Add(t);
        }

        public void Update(Task t)
        {
            taskRepository.Update(t);
        }

        public TaskStatu GetTaskStatuById(int id)
        {
            return taskStatusRepository.GetById(id);
        }

        public List<TaskStatu> GetAllTaskStatus()
        {
            return taskStatusRepository.GetAll().ToList();
        }

        public List<Employee> GetAllEmployee()
        {
            return employeeRepository.GetAll().Where(s => s.RoleId == 2).ToList();
        }

        public void Add(Task task)
        {
            taskRepository.Add(task);
        }

        private List<Assignment> GetAssignByTaskId(int id)
        {
            return assignmentRepository.GetAssignByTaskId(id);
        }

        public void RemoveAssignByTaskId(int id)
        {
            List<Assignment> list = GetAssignByTaskId(id);
            assignmentRepository.RemoveRange(list);
        }

        public void AddFileManager(FileManager f)
        {
            fileManagerRepository.Add(f);
        }

        public List<FileManager> GetAllFileManager()
        {
            return fileManagerRepository.GetAll().ToList();
        }

        public bool isFileNameDuplicate(string path)
        {
            List<FileManager> list = fileManagerRepository.GetAll().ToList();
                foreach(FileManager item in list)
                {
                    if (item.C_Path == path)
                    return true;
                }
            return false;
        }

        public string createFileName(int n, List<FileManager> list, string fileName, string extention, string fullName)
        {
            if (isFileNameDuplicate(fullName))
            {
                string fileRename = fileName + " (" + n.ToString() + ")";
                n++;
                fullName = fileRename + extention;
                fullName = createFileName(n, list, fileName, extention,fullName);
            }
            return fullName;
        }

        public string writeTaskLogWhenAddFile(int id, int role, int userId,List<string> listFileName)
        {
            if(listFileName != null)
            {
                string result = "";

                string fileNames = "";
                foreach (string item in listFileName)
                {
                    fileNames += item + ", ";
                }
                fileNames = fileNames.Remove(fileNames.Length -2);
                if (role == 1)
                {
                    result = "+ Quản trị viên đã thêm file " + fileNames + "\n";
                }
                else
                {
                    result = "+ Nhân viên đã thêm file " + fileNames + "\n";
                }
                return result;
            }
            return null;
        }

        public void DeleteFileManagerById(int fileId)
        {
            FileManager file = fileManagerRepository.GetById(fileId);
            fileManagerRepository.Remove(file);
        }

        public void writeTaskLogWhenDeleteFile(int fileId, int role, int userId,int taskId)
        {
            FileManager file = fileManagerRepository.GetById(fileId);

            TaskLog tl = new TaskLog();
            tl.C_TimeStamp = DateTime.Now;
            tl.EmployeeId = userId;
            tl.TaskId = taskId;
            if (role == 1)
            {
                tl.Content = "+ Quản trị viên đã xóa file " + file.C_Path;
            }
            else
            {
                tl.Content = "+ Nhân viên đã xóa file " + file.C_Path;
            }

            taskLogRepository.Add(tl);

        }

        private List<FileManager> getFileManagerByTaskId(int? id)
        {
            return fileManagerRepository.getByTaskId(id);
        }

        public TaskEditVM getTaskEditVM(int? id)
        {
            TaskEditVM vm = new TaskEditVM();

            Task t = taskRepository.GetById(id.GetValueOrDefault());

            vm.EndDate = t.EndDate;
            vm.StartDate = t.StartDate;
            vm.Scope = t.Scope;
            vm.StatusId = t.StatusId;
            vm.Title = t.Title;
            vm.ListFile = getFileManagerByTaskId(id);

            return vm;
        } 
       


    }
}