﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ToDoList.Models;
using ToDoList.Repositories;
using ToDoList.ViewModel;

namespace ToDoList.Services
{
    public class StatisticalService
    {
        private TaskRepository taskRepository;
        int pageSize = 3;
        public StatisticalService()
        {
            taskRepository = new TaskRepository();
        }


        public StatisticalIndexVM loadStatiscalExpiredTask(DateTime startDate, DateTime endDate, int pageIndex)
        {
            var statiscalCriteria = taskRepository.loadStatiscalExpiredTask(startDate, endDate);
            return new StatisticalIndexVM
            {
                statisticalExpiredTasks = PaginatedList<StatiscalExpiredTaskModel>
                                        .Create(statiscalCriteria.ToList(), pageIndex, pageSize)
            };
        }

        public StatisticalIndexVM loadStatiscalTaskByStatus(DateTime startDate, DateTime endDate, int pageIndex, int statusId)
        {
            var statiscalCriteria = taskRepository.loadStatiscalTaskByStatus(startDate, endDate, statusId);
            return new StatisticalIndexVM
            {
                statisticalTasks = PaginatedList<StatiscalTaskByStatusModel>
                                        .Create(statiscalCriteria.ToList(), pageIndex, pageSize)
            };
        }

        public StatisticalIndexVM loadDefaultStatistical(DateTime startDate, DateTime endDate, int pageIndex)
        {
            var initData = taskRepository.loadDefaultStatistical(startDate, endDate);
            return new StatisticalIndexVM
            {
                statisticalTasks = PaginatedList<StatiscalTaskByStatusModel>
                                      .Create(initData.ToList(), pageIndex, pageSize)
            };
        }

        public TaskIndexVM loadAllTaskOfUser(DateTime startDate, DateTime endDate, int userId, int pageIndex)
        {
            var initData = taskRepository.getTaskOfUserInPeriod(startDate, endDate, userId).Where(t => t.StatusId == 3);
            return new TaskIndexVM
            {
                Tasks = PaginatedList<Task>
                                      .Create(initData.ToList(), pageIndex, pageSize)
            };
        }
    }
}