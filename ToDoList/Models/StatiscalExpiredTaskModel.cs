﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class StatiscalExpiredTaskModel
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int NumberOfTaskExpired { get; set; }
    }
}