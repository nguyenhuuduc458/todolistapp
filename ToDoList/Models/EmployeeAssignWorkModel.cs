﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class EmployeeAssignWorkModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int roleId { get; set; }
    }
}