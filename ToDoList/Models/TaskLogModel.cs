﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class TaskLogModel
    {
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTime Timestamp { get; set; }
    }
}