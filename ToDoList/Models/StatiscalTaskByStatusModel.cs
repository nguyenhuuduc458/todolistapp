﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.Models
{
    public class StatiscalTaskByStatusModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public bool Scope { get; set; }
        public string Creator { get; set; }
        public int statusId { get; set; }

    }
}