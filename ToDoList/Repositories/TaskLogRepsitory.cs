﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoList.Models;

namespace ToDoList.Repositories
{
    public class TaskLogRepsitory : Repository<TaskLog>
    {
        public TaskLogRepsitory() : base() {}
        public ToDoListEntities context
        {
            get { return Context as ToDoListEntities; }
        }
    }
}