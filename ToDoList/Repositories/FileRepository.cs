﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoList.Models;

namespace ToDoList.Repositories
{
    public class FileManagerRepository : Repository<FileManager>
    {
        public FileManagerRepository() : base() { }

        public ToDoListEntities context
        {
            get { return Context as ToDoListEntities; }
        }

        public List<FileManager> getByTaskId(int? id)
        {
            int taskID = id.GetValueOrDefault();
            var result = from f in context.FileManagers
                         where f.TaskId == taskID
                         select f;
            return result.ToList();
        }
    }
}