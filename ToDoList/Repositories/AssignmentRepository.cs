﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoList.Models;

namespace ToDoList.Repositories
{
    public class AssignmentRepository : Repository<Assignment>
    {
        public AssignmentRepository() : base() { }
        public ToDoListEntities context
        {
            get { return Context as ToDoListEntities; }
        }

        public List<Employee> GetEmployeeByTaskId(int id)
        {
            var result = from e in context.Employees
                         join a in context.Assignments
                         on e.Id equals a.EmployeeId
                         join r in context.Roles
                         on e.RoleId equals r.Id
                         where a.TaskId == id && r.Id == 2
                         select e;
            return result.ToList();
        }

        public List<Assignment> GetAssignByTaskId(int id)
        {
            var result = from a in context.Assignments
                         join e in context.Employees
                         on a.EmployeeId equals e.Id
                         join r in context.Roles
                         on e.RoleId equals r.Id
                         where a.TaskId == id && r.Id == 2
                         select a;
            return result.ToList();
        }
    }
}