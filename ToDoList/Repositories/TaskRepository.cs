﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using ToDoList.Models;

namespace ToDoList.Repositories
{
    public class TaskRepository : Repository<Task>
    {
        public TaskRepository() : base() { }

        public ToDoListEntities context
        {
            get { return Context as ToDoListEntities; }
        }

        public IEnumerable<Task> getTaskOfUser(int userId)
        {
            var tasks = from a in context.Assignments
                        join t in context.Tasks
                        on a.TaskId equals t.Id
                        where a.EmployeeId == userId
                        select t;

            return tasks;
        }

        public IEnumerable<Task> getTaskOfUserInPeriod(DateTime startDate, DateTime endDate, int userId)
        {
            var tasks = from a in context.Assignments
                        join t in context.Tasks
                        on a.TaskId equals t.Id
                        where a.EmployeeId == userId && DbFunctions.TruncateTime(t.StartDate) >= DbFunctions.TruncateTime(startDate) && DbFunctions.TruncateTime(t.EndDate) <= DbFunctions.TruncateTime(endDate)
                        select t;

            return tasks;
        }

        /*DETAIL PAGE*/
        public List<EmployeeAssignWorkModel> getEmployeeAssignToTask(int taskId)
        {
            var employees = from e in context.Employees
                            join a in context.Assignments
                            on e.Id equals a.EmployeeId
                            where a.TaskId == taskId
                            select new EmployeeAssignWorkModel
                            {
                                Id = e.Id,
                                Name = e.Name,
                                roleId = e.RoleId
                            };
            return employees.ToList();
        }

        public List<FileManager> loadFileOfTask(int taskId)
        {
            var files = from f in context.FileManagers
                        where f.TaskId == taskId
                        select f;
            return files.ToList();
        }

        public IEnumerable<CommentModel> loadCommentOfTask(int taskId)
        {
            var comments = from m in context.Comments
                           join e in context.Employees
                           on m.EmployeeId equals e.Id
                           where m.TaskId == taskId 
                           select new CommentModel
                           {
                               Name = e.Name,
                               Role = e.RoleId,
                               Content = m.Content,
                               TimeStamp = m.C_TimeStamp
                           };

            return comments.AsEnumerable().Reverse().ToList();
        }

        public List<TaskLogModel> loadTaskLogOfTask(int taskId)
        {
            var comments = from t in context.TaskLogs
                           join e in context.Employees
                           on t.EmployeeId equals e.Id
                           where t.TaskId == taskId
                           select new TaskLogModel
                           {
                               Name = e.Name,
                               Content = t.Content,
                               Timestamp = t.C_TimeStamp
                           };
            return comments.OrderByDescending(t => t.Timestamp).ToList();
        }

        /*STATISTICAL PAGE*/
        public string findCreatorOfTask(int taskId)
        {
            var creators = context
                            .Assignments
                            .Where(t => t.TaskId == taskId)
                            .Select(e => e.Employee)
                            .Select(e => e.Name);
            return creators.First().ToString();
        }

        public IEnumerable<StatiscalTaskByStatusModel> loadStatiscalTaskByStatus(DateTime startDate, DateTime endDate, int statusId)
        {
            var criteriaStatistical = from t in context.Tasks
                                      where (DbFunctions.TruncateTime(t.StartDate) >= DbFunctions.TruncateTime(startDate) && DbFunctions.TruncateTime(t.EndDate) <= DbFunctions.TruncateTime(endDate)) 
                                      && t.StatusId == statusId
                                      select new StatiscalTaskByStatusModel
                                      {
                                          Id = t.Id,
                                          Title = t.Title,
                                          StartDate = t.StartDate,
                                          EndDate = t.EndDate,
                                          Scope = t.Scope,
                                          statusId = t.StatusId

                                      };

            List<StatiscalTaskByStatusModel> result = criteriaStatistical.ToList();
           
            //set creator for each task
            foreach (StatiscalTaskByStatusModel model in result)
            {
                int taskId = model.Id;
                model.Creator = findCreatorOfTask(taskId);
            }

            return result.AsEnumerable();
         }

        public IEnumerable<StatiscalTaskByStatusModel> loadDefaultStatistical(DateTime startDate, DateTime endDate)
        {
            var criteriaStatistical = from t in context.Tasks
                                      where (DbFunctions.TruncateTime(t.StartDate) >= DbFunctions.TruncateTime(startDate) && DbFunctions.TruncateTime(t.EndDate) <= DbFunctions.TruncateTime(endDate))
                                      select new StatiscalTaskByStatusModel
                                      {
                                          Id = t.Id,
                                          Title = t.Title,
                                          StartDate = t.StartDate,
                                          EndDate = t.EndDate,
                                          Scope = t.Scope,
                                          statusId = t.StatusId

                                      };

            List<StatiscalTaskByStatusModel> result = criteriaStatistical.ToList();

            //set creator for each task
            foreach (StatiscalTaskByStatusModel model in result)
            {
                int taskId = model.Id;
                model.Creator = findCreatorOfTask(taskId);
            }

            return result.AsEnumerable();
        }

        public IEnumerable<StatiscalExpiredTaskModel> loadStatiscalExpiredTask(DateTime startDate, DateTime endDate)
        {
            var expiredTask = context.Employees.Select(e => new StatiscalExpiredTaskModel
            {
                EmployeeId = e.Id,
                Name = e.Name,
                NumberOfTaskExpired = e.Assignments.Where(a => a.EmployeeId == e.Id)
                                    .Select(r => r.Task)
                                    .Where(t => DbFunctions.TruncateTime(t.StartDate) >= DbFunctions.TruncateTime(startDate)
                                        && DbFunctions.TruncateTime(t.EndDate) <= DbFunctions.TruncateTime(endDate)
                                        && t.StatusId == 3)
                                    .Count()
            });
            return expiredTask;
        }
    }
}