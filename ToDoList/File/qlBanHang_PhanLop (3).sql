﻿CREATE DATABASE qlBanHang_PhanLop
USE qlBanHang_PhanLop

CREATE TABLE LoaiSanPham(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Ten NVARCHAR(200) NOT NULL
)

CREATE TABLE SanPham(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Ten NVARCHAR(200) NOT NULL,
	Gia INT NOT NULL,
	GioiThieu NTEXT NULL,
	ImageUrl TEXT NULL,
	SoLuongTon INT NOT NULL,
	IdLoaiSanPham INT NOT NULL
	FOREIGN KEY REFERENCES LoaiSanPham(Id)
)

CREATE TABLE Combo(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Ten NVARCHAR(100) NOT NULL,
	Gia INT NOT NULL,
	ImageUrl TEXT NULL
)

CREATE TABLE ComboSanPham(
	IdComBo INT NOT NULL
	FOREIGN KEY REFERENCES Combo(Id), 
	IdSanPham INT NOT NULL
	FOREIGN KEY REFERENCES SanPham(Id),
	SoLuong INT NOT NULL,
	TongGia INT NOT NULL,
	PRIMARY KEY(IdCombo,IdSanPham)
)

CREATE TABLE LogSoLuongTon(
	Id INT PRIMARY KEY IDENTITY(1,1),
	ThoiGianThayDoi DATETIME NOT NULL,
	SoLuongTon INT NOT NULL,
	IdSanPham INT NOT NULL
	FOREIGN KEY REFERENCES SanPham(Id)
)

CREATE TABLE NhanVien(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Ten NVARCHAR(100) NOT NULL,
	UserName VARCHAR(100) NOT NULL,
	_Password VARCHAR(100) NOT NULL
)

CREATE TABLE KhachHang(
	Id INT PRIMARY KEY IDENTITY(1,1),
	Ten NVARCHAR(100) NOT NULL,
	UserName VARCHAR(100) NOT NULL,
	_Password VARCHAR(100) NOT NULL,
	DiaChi NVARCHAR(200) NULL
)

CREATE TABLE HoaDon(
	Id INT PRIMARY KEY IDENTITY(1,1),
	NgayLapHD DATETIME NOT NULL,
	IdNhanVien INT NOT NULL
	FOREIGN KEY REFERENCES NhanVien(Id),
	IdKhachHang INT NOT NULL
	FOREIGN KEY REFERENCES KhachHang(Id)
)

CREATE TABLE ChiTietHoaDon_SanPham(
	IdHoaDon INT NOT NULL
	FOREIGN KEY REFERENCES HoaDon(Id),
	IdSanPham INT NOT NULL
	FOREIGN KEY REFERENCES SanPham(Id),
	SoLuong INT NOT NULL,
	TongTien INT NOT NULL,
	PRIMARY KEY (IdHoaDon,IdSanPham)
)

CREATE TABLE ChiTietHoaDon_Combo(
	IdHoaDon INT NOT NULL
	FOREIGN KEY REFERENCES HoaDon(Id),
	IdCombo INT NOT NULL
	FOREIGN KEY REFERENCES Combo(Id),
	SoLuong INT NOT NULL,
	TongTien INT NOT NULL,
	PRIMARY KEY (IdHoaDon,IdCombo)
)

INSERT INTO LoaiSanPham VALUES(N'Laptop'),
							  (N'Máy bộ'),
							  (N'Linh kiện'),
							  (N'Phụ kiện')

INSERT INTO SanPham VALUES(N'Laptop Dell Inspiron 15 7501 X3MRY1',29000000,N'Laptop sử dụng công nghệ 7nm hiện đại, trang bị tối đa tới vi xử lí AMD Ryzen 5 4600H và card đồ họa Radeon™ RX5300M 3GB, đem tới cho chiếc laptop khi chơi game có hiệu năng ngang tầm máy desktop.Hãy tận hưởng trải nghiệm chơi game và giải trí đa phương tiện tuyệt hảo. Sử dụng RAM 8G DDR4 3200 cho tốc độ tải dữ liệu nhanh vượt trội. Hiệu năng được cải thiện khoảng 10% so với DDR4-2400. AMD FreeSync™ Premium giúp các game thủ hạng nặng chơi game cực mượt, không bị xé hình. Không còn phải đánh đổi yếu tố nào nữa, thoải mái chơi game với hiệu năng cao, giảm thiểu giật hình và độ trễ thấp.Nắp của chiếc laptop gaming này và phần bàn phím được làm bằng kim loại, kết hợp với thiết kế hiện đại, đem lại cảm hứng để bước vào chiến trường ảo.Với cụm tản nhiệt riêng cho CPU và GPU sử dụng tổng cộng 7 ống dẫn nhiệt, kết hợp hiệu quả để tản nhiệt và tối ưu hóa luồng gió trong một thân máy siêu gọn. Sức mạnh cực đỉnh để làm việc và giải trí di động.Laptop Dell mang tới trải nghiệm âm thanh tốt nhất là với các bản thu lossless với chất lượng như âm thanh gốc.Chiếc laptop MSI này còn có phần mềm Dragon Center độc quyền của MSI giúp bạn điều chỉnh và tùy biến chiếc laptop theo ý muốn của bản thân. Theo dõi tình trạng hoạt động, điều chỉnh và tối ưu một cách dễ dàng trên một giao diện thống nhất.Được phát triển nhờ quan hệ hợp tác với BlueStacks, phần mềm MSI APP Player đem lại trải nghiệm chơi game mobile mượt mà trên nền tảng PC. Nó cho phép game thủ tùy chỉnh nhiều tính năng, như đèn nền bàn phím, khả năng xử lí đồ họa và chạy đa nhiệm.Bravo 15 có một cổng LAN RJ45, hai cổng Type-C USB 3.2 Gen 1, hai cổng Type-A USB 3.2 Gen 1 và một cổng HDMI, giúp kết nối cực kì dễ dàng.','',100,1),
						  (N'Laptop Asus TUF Gaming FA506II AL016T',22000000,N'Laptop ASUS Expertbook P1410CJA EK355T mang phong cách thiết kế tối giản, lịch lãm với các đường nét được bo tròn nhẹ nhàng cùng màu đen sao. Khung máy được gia cố chắc chắn cùng công nghệ bảo vệ ổ cứng EAR, bảo vệ dữ liệu khỏi các tác động vật lý. Với màn hình 14 inch cùng khối lượng chỉ 1.442kg. Máy khá gọn nhẹ, khả năng di động cao thích hợp để bạn mang đi khắp mọi nơi.Laptop ASUS Expertbook P1410CJA EK355T sở hữu bộ vi xử lý chip Intel i5 đời 10 mới nhất cho khả năng xử lý mọi tác vụ văn phòng nhanh gọn, kể các tác vụ yêu cầu tính toán phức tạp. Bộ nhớ RAM 8GB đa nhiệm, cùng lúc xử lý nhiều công việc hay mở nhiều tab Chrome không giật, lag. Ổ cứng SSD 512GB có tốc độ đọc ghi siêu nhanh, mở máy, khởi động phần mềm không độ trễ.Bàn phím mang thiết kế công thái học, kết cấu liền khối vững chắc của máy cùng với hành trình phím 1,4mm mang đến cảm giác gõ phím hoàn toàn thoải mái. Đèn led phím hỗ trợ làm việc tốt trong điều kiện ánh sáng yếu. Touchpad rộng rãi, nhanh nhạy, với những tác vụ thông thường sẽ không cần đến sự hỗ trợ của chuột rời. ASUS Expertbook P1410CJA EK355T còn được trang bị đầy đủ các cổng kết nối cần thiết, tiện lợi cho việc chia sẻ thông tin, dữ liệu hay kết nối với các thiết bị ngoại vi như cổng HDMI, đầu đọc thẻ SD, USB 2.0, cổng USB type C 3.1 cho phép kết nối 2 chiều,...','',100,1),
						  (N'Laptop HP Envy 13-ba1031TU 2K0B7PA',25000000,N'Laptop HP ENVY 13-ba1031TU 2K0B7PA sở hữu một thiết kế trẻ trung, năng động chắc chắn sẽ mang đến cho bạn sự tự tin nhất khi cầm trên tay. Đây là chiếc laptop kết hợp hoàn hảo của một chiếc laptop văn phòng với thời trang hiện đại. Được thiết kế theo xu hướng chuẩn hiện đại tương tự như các dòng laptop ultrabook hiện nay với kích thước mỏng hơn và nhẹ hơn. Khung máy bền bỉ với tông màu vàng lịch lãm sẽ đem lại một phong cách chuyên nghiệp cho bạn.Được trang bị con chip Intel® Core™ i7-1165G7 thế hệ mới nhất với xung nhịp 4.70GHz bạn có thể xử lý các tác vụ hằng ngày, công việc văn phòng, xem phim, lướt web một cách đơn giản. Bên cạnh đó, máy tính có 16GB RAM và 1TB PCIe® NVMe™ M.2  dung lượng SSD để cung cấp cho người dùng trải nghiệm đa nhiệm mượt mà, lưu trữ dữ liệu thoải mái và tốc độ truyền tải nhanh chóng.Sở hữu màn hình laptop 13.3 inch FullHD, hiển thị hình ảnh rõ nét, sống động. Công nghệ màn hình IPS sẽ cho bạn thưởng thức những bức ảnh màu sắc tươi sáng và rực rỡ nhất mà không gây khó chịu cho mắt.HP đã nghiên cứu và trang bị bàn phím hiện đại cùng TouchPad rộng trên chiếc máy tính này nhằm giúp người dùng có trải nghiệm nhập dữ liệu tối ưu. Khoảng cách giữa các phím bấm hợp lý, độ nảy cao và hành trình phím sâu tạo cảm giác nhập liệu nhanh chóng và thoải mái.Bàn di chuột TouchPad với diện tích lớn, hỗ trợ bạn sử dụng cảm ứng đa điểm để cuộn văn bản, xoay hình ảnh và nhiều thao tác tiện lợi.Để đáp ứng nhu cầu liên kết với các thiết bị ngoại vi, được trang bị đa dạng các cổng I/O. Ở cạnh bên phải là jack nguồn, 2 USB 3.2 Type A và combo tai nghe/micro 3.5mm. Ở cạnh trái đầy đủ hơn với HDMI 2.1, USB 3.1 Type A, Ethernet, USB 3.1 Type C và đầu đọc thẻ SD.','',100,1),
						  (N'Laptop Asus D509DA EJ116T',9500000,N'ASUS ExpertBook P2451FA EK0229T là sản phẩm hoàn hảo để mang lại hiệu toàn diện để giúp bạn vượt qua một ngày làm việc. Máy tính xách tay siêu nhẹ kết hợp cùng bản lề phẳng 180 độ để bạn có thể đặt laptop trên bất kỹ mặt phẳng nào, cực kỳ lý tưởng để chia sẻ nội dung trên màn hình với người khác.ExpertBook P2 kết hợp các yếu tố kiến trúc vào thiết kế với lớp hoàn thiện Star Black bóng bẩy và các góc được tạo hình chính xác, mang đến vẻ ngoài thanh lịch và chuyên nghiệp hơn hẵn so với các mẫu laptop giá rẻ khác.Máy có kích thước siêu mỏng và nhẹ 1,5kg cho phép bạn có thể dễ dàng cho vào một chiếc cặp hoặc ba lô để bạn có thể làm việc ở bất cứ đâu.Một màn hình NanoEdge viền mỏng cung cấp không gian làm việc rộng hơn. Tỉ lệ màn hình so với khung máy cũng giúp phần khiến cho mẫu laptop 14inch này nhìn không quá khác so với các dòng máy tính xách tay 13inch hiện nay.Asus ExpertBook P2 được thiết kế cho các doanh nhân cần hiệu năng ổn định để thực hiện công việc hàng ngày. Bộ xử lý Intel Core i5 thế hệ mới nhất mang đến hiệu năng nhanh và nhạy mà bạn cần. Với thiết kế lưu trữ kép cung cấp không gian lưu trữ rộng rãi và truy cập dữ liệu nhanh hơn. Laptop Asus ExpertBook P2 cũng là lựa chọn thích hợp trong tương lai, với thiết kế dễ truy cập giúp dễ dàng nâng cấp các thành phần bên trong để phục vụ nhiều nhu cầu làm việc hơn nữa.Khi bạn làm việc ở những nơi công cộng, kết nối là rất quan trọng. ExpertBook P2 cung cấp một bộ cổng I/O toàn diện để truyền dữ liệu dễ dàng và kết nối ngoại vi linh hoạt. Để đảm bảo độ tin cậy, các cổng được kiểm tra để chịu đựng tới 15.000 lần.Thiết kế khung gầm kết hợp các tính năng cung cấp thêm độ cứng cho kết cấu để đối phó với các trường hợp không may có thể diễn ra bất cứ lúc nào.Độ bền cuối cùng là giá trị cốt lõi mà ExpertBook P2 mang lại cho người dùng doanh nghiệp, đảm bảo tài sản có giá trị của bạn sẽ tồn tại bất cứ điều gì nghiêm ngặt hàng ngày, tác động ngẫu nhiên mà họ gặp phải. ExpertBook P2 được xây dựng chắc chắn và được kiểm tra chất lượng nghiêm ngặt theo tiêu chuẩn quân sự MIL-STD 810G của Hoa Kỳ với các bài kiểm tra chất lượng của ASUS.ExpertBook P2 bao gồm các tính năng bảo mật toàn diện cho doanh nghiệp để bảo vệ dữ liệu quan trọng của bạn. Một khe khóa Kensington bảo vệ chống trộm, trong khi tấm chắn bảo mật webcam, cảm biến vân tay và chip TPM 2.0 giữ cho những gì bên trong an toàn. Nếu bạn đang băn khoăn lo lắng về vấn đề bảo mật thì hãy chọn ngay cho mình sản phẩm laptop Asus này nhé!','',100,1),
						  (N'G-Station K303',35000000,N'Được tối ưu cấu hình hoàn hảo cho trải nghiệm sử dụng bộ phần mềm Adobe ở cấp độ nhân viên/chuyên viên. Bộ máy này phục vụ tốt cho những công việc: Diễn họa viên, chuyên viên thiết kế đồ họa sử dụng các phần mềm Adobe Photoshop, Adobe Lightroom, Adobe Illustrator, … Sinh viên chuyên ngành thiết kế sử dụng các phần mềm thiết kế đồ họa 2D, 3D modeling: Adobe Dimension, Substance, ... Phóng viên, thợ chụp ảnh nghiệp dư, bán chuyên sử dụng các phần mềm Adobe Lightroom, Adobe Photoshop, … Biên tập viên, Editor Video sử dụng các phần mềm Adobe Premiere, Adobe After Effects, …Sử dụng nền tảng Vi xử lý từ Intel với độ ổn định cực kì cao và khả năng nâng cấp mạnh mẽ','',100,2),
						  (N'G-Creator C502',40000000,N'Về các tính năng chính,Mainboard ASUS Prime X570-P có hai khe cắm PCIe 4.0 có độ dài đầy đủ hoạt động ở x16 và x16 / x4. Điều này là do bộ xử lý sê-ri Ryzen 3000 cung cấp khe cắm trên cùng, trong khi các làn khe dài đầy đủ phía dưới đến trực tiếp từ chipset X570. Hỗ trợ cho cấu hình card đồ họa đa chiều AMD CrossFire hai chiều, nhưng không hỗ trợ cho NVIDIA SLI. Đối với các thiết bị lưu trữ, có hai khe M.2 hỗ trợ các ổ PCIe 4.0 x4, nhưng người dùng muốn chạy các ổ NVMe chạy nhanh và nóng có thể cần phải mua tản nhiệt riêng vì bo mạch không có tính năng bao gồm trên Prime X570-P có tổng cộng sáu cổng SATA. Hỗ trợ bộ nhớ rất tốt với bốn khe cắm bộ nhớ với hỗ trợ lên tới 128 GB với UDIMM 32 GB đã được ASUS đủ điều kiện trong ngăn xếp sản phẩm X570 của mình. CPU AMD Ryzen 9 3900X đánh đúng tâm lý người dùng với một CPU đa nhân giá cả hợp lý đủ mạnh mẽ để phục vụ công việc và dĩ nhiên là thừa sức chiến tất cả các tựa game. Không chỉ đơn thuần là một chiếc CPU nhiều nhân, CPU AMD Ryzen 9 3900X còn sử dụng kiến trúc Zen 2 mới giúp cải thiện hiệu năng của từng nhân, kết hợp với tiến trình sản xuất 7 nm để tối ưu điện năng tiêu thụ. Chính vì vậy mà ngay cả khi tích hợp đến 12 nhân, điện năng tiêu thụ của CPU AMD Ryzen 9 3900X vẫn trong tầm kiểm soát và gần như cao hơn không đáng kể so với Ryzen 2700X 8 nhân/16 luồng trước đó. G.SKILL Trident Z RGB DDR4 Bus 3000 vẫn giữ nguyên thiết kế mang tính biểu tượng của dòng sản phẩm Trident Z truyền thống - có bộ tản nhiệt bằng nhôm sang trọng giúp tản nhiệt cực tốt. Được trang bị dải led RGB 16 triệu màu, hỗ trợ tùy chỉnh dễ dàng bằng phần mềm. Bên cạnh đó, còn được trang bị các chip nhớ IC đã được sàng lọc đặc biệt thông qua quá trình lựa chọn tuyệt vời của G.SKILL và một PCB mười lớp được thiết kế tùy chỉnh cung cấp độ ổn định tín hiệu tối đa, tương thích và hiệu suất trên rất nhiều các bo mạch chủ.','',100,2),
						  (N'G-Creator C702',70000000,N'CPU Intel Core i9 10900K là bộ vi xử lý hàng đầu trong dòng sản phẩm Intel Comet Lake thế hệ thứ 10 và sản phẩm này cũng đánh dấu lần đầu tiên Intel đã đưa 10 lõi xử lý vào một bộ xử lý chính. i9 10900K sở hữu 10 nhân 20 luồng với tốc độ xử lý cơ bản 3.7GHz và tối đa có thể lên đến 5.3GHz kết hợp cùng công nghệ Heat Velocity Boost hiện đại. Nếu bạn có nhu cầu tìm làm việc với các tác vụ đồ họa cao cấp hay cần tìm cho mình một bộ PC có khả năng dựng - render 4K một cách mượt mà thì với i9 10900K, bạn có thể yên tâm tuyệt đối với hiệu năng mà CPU mang lại. Asus vừa cho ra mắt series bo mạch chủ Z490 cao cấp. Mainboard ASUS ROG Z490 MAXIMUS XII EXTREME sử dụng mô-đun cấp điện 16 pha có khả năng xử lý lên đến 90 ampe. Công nghệ ép xung thông minh - ProCool II giúp cải thiện đáng kể khả năng ép xung, nhất là khi kết hợp cùng bộ vi xử lý cao cấp i9 10900K đến từ Intel.  Để tăng hiệu quả tản nhiệt, thiết kế ống dẫn nhiệt chữ U có thể làm giảm đáng kể nhiệt độ khi CPU hoạt động ở công suất cao. Khu vực làm mát bằng nước của Z490 MAXIMUS XII EXTREME cho phép người dùng có thể theo dõi tình trạng hoạt động của bo mạch chủ và có khả năng tự điều chỉnh để tránh gặp những sự cố xảy ra khi lắp đặt hoặc phải hoạt động liên tục trong thời gian dài. Để tối ưu khả năng xử lý đa nhiệm, GEARVN cũng trang bị trên dòng PC cao cấp nhất 32GB với bus 3000. Ngoài hỗ trợ làm việc với các tác vụ đa nhiệm, bề ngoài bắt mắt của G.SKILL Trident Z RGB cũng là một điểm cộng lớn giúp góc gaming của bạn càng nổi bật hơn rất nhiều.Card đồ họa MSI GeForce RTX 3090 GAMING X TRIO 24G chắc chắn là sự lựa chọn tốt nhất hiện nay cho fan ROG nếu riêng và game thủ nói chung. Hiệu năng mạnh mẽ của RTX 3090 hoàn toàn có thể đáp ứng mọi yêu cầu mà những tựa game đình đám có đồ họa thuộc hàng khủng nhất hiện nay như:Call of Duty: Modern Warfare, Red Dead Redemption 2,...','',100,2),
						  (N'RAM Kingston HyperX Fury Black',800000,N'Ram Desktop Kingston HyperX Fury RGB là dòng RAM lâu năm của Kingston với chất lượng đã được khẳng định. Phiên bản DDR4 thích hợp cho cả hệ thống Intel và AMD đi cùng tốc độ rất cao, đạt 3200Mhz và nhiều tính năng hấp dẫn. Tạo phong cách đặc sắc cho dàn máy tính PC của bạn với bộ tản nhiệt mới cho FURY DDR4 RGB và các hiệu ứng chiếu sáng mượt mà, và ấn tượng. Dải LED RGB có các hiệu ứng cực kỳ mượt mà, tuyệt đẹp và có thể tùy chỉnh theo ý thích. Bạn có thể tùy chỉnh hiệu ứng RGB bằng phần mềm HyperX NGENUITY đi kèm cực kỳ mạnh mẽ hoặc có thể đồng bộ cực kỳ dễ dàng bằng phần mềm của Mainboard hỗ trợ. Ram HyperX Fury với các hiệu ứng RGB sẽ luôn được đồng bộ nhờ công nghệ Infrared Sync đã được cấp bằng sáng chế của HyperX. Trang bị công nghệ HyperX Infrared Sync  giúp hệ thống của bạn đồng bộ hiệu ứng cực kỳ dễ dàng mà không cần phải cắm dây tín hiệu RGB. Quá trình tự động ép xung lên đến tần số cao nhất mà Mainboard hỗ trợ. Chỉ cần cắm và chạy, không cần tinh chỉnh trong Bios. RAM Kingston HyperX Fury RGB (8GB DDR4 1x8G 3200) trang bị hệ thống tản nhiệt bằng nhôm màu đen, vừa giúp hệ thống máy tính của bạn trông hầm hố hơn vừa có tác dụng giải nhiệt cực tốt, giúp hệ thống của bạn chạy mát mẻ hơn.','',100,3),
						  (N'HDD Seagate Barracuda 1TB 7200rpm',1000000,N'Ổ cứng di động SSD WD My Passport Go 500GB (WDBMCG5000ABT-WESN) được thiết kế và sản xuất bởi WD (tên đầy đủ là Western Digital) - là một công ty thiết kế, sản xuất và phát triển thiết bị lưu trữ dữ liệu và là một trong những nhà sản xuất ổ đĩa cứng lớn nhất thế giới có lịch sử lâu đời trong ngành công nghiệp điện tử.Với thiết kế theo dạng ổ cứng di dộng gắn ngoài với form 2,5 inch có kích thước 95 x 67 x 10 mm nhỏ gọn, tiện lợi, đáp ứng mọi nhu cầu lưu trữ của người dùng khi đang di chuyển cùng với vỏ ngoài gia cố chắc chắn bảo vệ ổ cứng khỏi các chấn động bất chợt, đảm bảo các linh kiện, dữ liệu bên trong được an toàn. Ổ cứng di động My Passport ™ Go cung cấp hiệu suất SSD, nhanh hơn 2 lần so với ổ đĩa cứng di động thông thường.  Được thiết kế với dung lượng lên đến 500GB giúp cho người dùng có thể thoải mái lưu trữ dữ liệu hình ảnh, video, âm thanh, tài liệu mà không phải lo nghĩ về vấn đề thiếu không gian lưu trữ. Ngoài ra, ổ cứng còn được hỗ trợ với phần mềm tự động back up dữ liệu hệ thống từ PC hay laptop. Ổ đĩa di động SSD WD My Passport Go 500GB có các cạnh cao su chống sốc và chắc chắn, làm cho nó có khả năng chống rơi đến 2 mét và giúp giữ cho nội dung của bạn an toàn khỏi va đập và rung. Thiế kế và sản xuất bởi Western Digital, một công ty lưu trữ hàng đầu, ổ đĩa My Passport ™ Go cung cấp cho bạn độ tin cậy và độ tin cậy mà bạn có thể tin tưởng. Yếu tố hình thức bỏ túi và cáp tích hợp giúp ổ đĩa My Passport Go cực kỳ tiện lợi để mang theo bên mình mọi lúc mọi nơi mà không để lại các thành phần quan trọng phía sau. Cho dù bạn sử dụng máy tính PC hay Mac, ổ cứng di động  SSD WD My Passport Go 500GB vẫn hoạt động dễ dàng ngay lập tức và bao gồm phần mềm sao lưu tự động 1 cho Windows và cũng tương thích với Time Machine cho sao lưu Mac (yêu cầu định dạng lại).','',100,3),
						  (N'Case XIGMATEK GEMINI (Mini Tower)',600000,N'Case MSI MPG GUNGNIR 110R được thiết kế với vẻ ngoài hiệu suất cao đáng nhớ để gây ấn tượng ngay từ cái nhìn đầu tiên. Một tấm kính phía trước kiểu dáng đẹp cung cấp một cái nhìn rực rỡ của ánh sáng bên trong, làm nổi bật chất lượng của các bộ phận trong phần còn lại của hệ thống. Kết hợp MPG GUNGNIR 110 Series với bo mạch chủ MSI bao gồm kết nối cổng USB Type-C để truy cập thuận tiện. Thiết lập một hệ sinh thái chơi game MSI hoàn chỉnh với card đồ họa và tản nhiệt chất lỏng. Nhanh chóng điều khiển màu sắc của quạt và các hiệu ứng ánh sáng với Insta-Light Loop chỉ bằng một nút bấm. Nhấp qua các hồ sơ một cách dễ dàng và chọn một yêu thích. MPG GUNGNIR 110 Series tiết lộ kho vũ khí của bạn với một mặt bên bằng kính cường lực. Trang bị hệ thống với bộ làm mát chất lỏng, bo mạch chủ và card đồ họa của MSI để có màn hình hiển thị sức mạnh và hiệu suất tối ưu.','',100,3),
						  (N'Tản Khí DEEPCOOL GAMMAXX 400 RED',500000,N'Các bộ làm mát CPU dòng DX của Noctua đã trở thành một lựa chọn mặc định trong các giải pháp làm mát yên tĩnh cao cấp cho bộ xử lý Intel Xeon. NH-U12S DX-3647 có bề mặt tiếp xúc lớn hơn, được thiết kế tùy chỉnh và hỗ trợ cả nền tảng LGA3647 vuông và hẹp. Dựa trên bộ tản nhiệt NH-U12S kích thước 120mm đã được chứng minh và được trang bị hai quạt 120mm được điều khiển bởi Noctua, điều khiển PWM, nó cung cấp một sự cân bằng tuyệt vời về hiệu suất, khả năng tương thích và sự yên tĩnh khi vận hành. Đứng đầu với hệ thống lắp SecuFirm2 ™ chuyên nghiệp và hợp chất nhiệt NT-H1 được áp dụng trước, NH-U12S DX-3647 tạo thành một gói chất lượng cao cấp hoàn chỉnh để làm mát lặng lẽ các máy trạm và máy chủ Xeon dựa trên LGA3647. Được giới thiệu lần đầu tiên vào năm 2008, các bộ làm mát CPU dòng DX của Noctua đã trở thành một lựa chọn tiêu chuẩn khi nói đến các giải pháp làm mát không khí cấp cao, yên tĩnh cho bộ xử lý Intel Xeon. Nhờ hiệu quả âm thanh vượt trội, chúng rất lý tưởng cho các máy trạm và máy chủ chạy trong môi trường nhạy cảm với tiếng ồn (ví dụ: sản xuất âm thanh / video, tạo nội dung, kỹ thuật, v.v.). Các bộ làm mát dòng DX-3647 mới là các giải pháp chuyên dụng, tùy chỉnh cho nền tảng LGA3647 chuyên nghiệp của Intel, giúp chúng phù hợp lý tưởng cho cả CPU Xeon có thể mở rộng dựa trên Skylake-SP (dòng Platinum, Gold, Silver hoặc Bronze, Purley) và bộ xử lý Xeon-Phi. Với kích thước 70x56mm, bề mặt tiếp xúc bằng đồng của tản nhiệt lớn hơn gấp đôi kích thước của mẫu tiêu chuẩn. Được thiết kế để phù hợp với các bộ tản nhiệt tích hợp khổng lồ (IHS) của bộ xử lý dựa trên LGA3647 của Intel, thiết kế tùy chỉnh này cho phép một luồng nhiệt tối ưu từ CPU qua đế đến ống tản nhiệt và vào vây làm mát. Với năm ống dẫn nhiệt và hai quạt 120mm thế hệ tiếp theo NF-A12x25, NH-U12S DX-3647 đạt được hiệu suất làm mát vượt trội trong khi vẫn duy trì khả năng tương thích tuyệt vời: Với chiều cao 158mm và chiều rộng 125mm, nó đủ ngắn để phù hợp với hầu hết các trường hợp tháp hiện đại và vẫn rõ ràng của khe cắm PCIe hàng đầu trên nhiều bo mạch chủ.','',100,3),
						  (N'Bàn phím Razer Blackwidow V3 Pro Green Switch',6000000,N'Bàn phím cơ Razer Blackwidow V3 Pro cơ học đầu tiên và mang tính biểu tượng nhất trên thế giới tạo nên sự phát triển mang tính bước ngoặt. Bước vào một meta không dây mới với Razer BlackWidow V3 Pro — với 3 chế độ kết nối mang lại tính linh hoạt vô song và trải nghiệm chơi game thỏa mãn bao gồm các công tắc tốt nhất trong lớp và các phím có chiều cao đầy đủ. Razer Blackwidow V3 Pro Green Switch - bàn phím bluetooth cơ học được trang bị công nghệ không dây tiên tiến nhất của chúng tôi để chơi game có độ trễ thấp và đầu vào siêu phản hồi — được thực hiện thông qua giao thức dữ liệu được tối ưu hóa, tần số vô tuyến cực nhanh và chuyển đổi tần số liền mạch trong môi trường ồn ào nhất, bão hòa dữ liệu. Razer Blackwidow V3 Pro Green Switch sử dụng Razer HyperSpeed ​​để có hiệu suất không dây hoàn hảo khi chơi game hoặc chuyển sang Bluetooth và kết nối tối đa 3 thiết bị — chuyển đổi liền mạch giữa chúng chỉ với một công tắc. Bao gồm cáp USB-C có thể tháo rời để sạc trong quá trình sử dụng. Lắng nghe và cảm nhận phản hồi hài lòng trong mỗi lần nhấn phím bạn thực hiện, một trong những chiếc bàn phím cơ giá rẻ với thiết kế nhạy bén, xúc giác cung cấp các điểm khởi động và đặt lại được tối ưu hóa để có độ chính xác và hiệu suất tốt hơn khi chơi game. Bàn phím Razer Blackwidow V3 Pro được thiết kế hoàn toàn rõ ràng của nó cung cấp ánh sáng RGB sáng hơn để hiển thị sự rực rỡ thực sự của những gì Razer Chroma ™ RGB có thể làm — từ các tùy chỉnh ánh sáng sâu đến đắm chìm hơn khi nó phản ứng động với hơn 150 trò chơi tích hợp. Cá nhân hóa bàn phím chơi game RGB này với hơn 16,8 triệu màu và bộ hiệu ứng để bạn lựa chọn. Tận hưởng cảm giác đắm chìm hơn với các hiệu ứng ánh sáng động xảy ra khi bạn chơi game trên hơn 150 tựa game được tích hợp Chroma như Fortnite, Apex Legends, Warframe,... Bàn phím Razer Blackwidow V3 Pro sử dụng quy trình đúc Doubleshot để đảm bảo không bao giờ bị mòn, các keycaps trên bàn phím cơ chơi game không dây này cũng có thành cực dày làm cho chúng cực kỳ cứng chắc để chịu được việc sử dụng nhiều lần.','',100,4),
						  (N'Tai nghe SteelSeries Arctis Pro Wireless',8500000,N'Tai nghe SteelSeries Arctis Pro Wireless với các trình điều khiển loa kết hợp công nghệ mà hãng sản xuất tạo ra được sản phẩm không dây nhưng có độ trễ cực kì ngắn, mang âm thanh lớn tới tai nghe của bạn. Đây là một sự giải pháp dành cho giới game thủ muốn có một chiếc tai nghe không dây chất lượng trong từng trận đấu. Với tính năng âm thanh không dây 2.4G. Tai nghe tạo ra được âm thanh không giới hạn kết hợp với Bluetooth cho các thiết bị di động. Sử dụng cả hai kết nối độc lập tạo nên sự đồng nhất tối đa linh hoạt. Tai nghe gaming giá rẻ Artics Pro kết nối với đầu phát sóng không dây thông qua kết nối SteelSeries 2.4G đã được chứng thực. Điều này khiến cho chiếc tai nghe có âm thanh cực kì tốt và ảnh hưởng cực kì ít khi ở độ xa lên tới 40 feet. Với 2 loại pin đi kèm nhau nghĩa rằng bạn sẽ luôn được sử dụng tai nghe đến “ vô thời hạn “ mà không lo hết pin. Đơn giản chỉ cần sử dụng một chiếc và chiếc kia cắm sạc là bạn sẽ không bao giờ phải đợi hàng giờ để được sử dụng chiếc tai nghe yêu quý này.','',100,4),
						  (N'Màn hình Asus ROG Swift PG65UQ 65" VA 4K 144Hz G-Sync',170000000,N'Kích thước 35” độ cong hoàn hảo 1800R, tỷ lệ 21:9 Độ phân giải UWQHD 3440 x 1440, trên tấm nền IPS 10 bit cao cấp cùng công nghệ Quantum dot => mang đến hình ảnh hiển thị trung thực, độ chuẩn xác cao. Công nghệ HDR, công nghệ chấm lượng tử Quantum dot cùng khả năng điều khiển đèn nền LED trên 512-zone khác nhau (nâng cấp so với X27 384-zone) => hình ảnh hiển thị không bị rỗ, mà vô cùng mịn, màu trắng vô cùng trong trẻo, và đen tuyền, đồng thời cho dải màu rộng hơn, sắc nét => độ tương phản cao 2500:1 gấp đôi màn hình thường Khả năng hiển thị 1.07 tỷ màu, chuẩn 90% DCI-P3: dải màu chuẩn trong công nghệ ảnh kỹ thuật số, rộng hơn dải màu sRGB 25%. Đạt chuẩn hiển thị VESA Display 1000, mang đến độ sáng tối đa 1000nits, gấp 4 lần màn hình bình thường Công nghệ HDR mang lại hình ảnh có nét tương phản cao hơn cùng màu sắc sống động hơn. Khả năng Overclock lên đến 200Hz - tần số quét đỉnh cao cùng thời gian phản hồi cực nhanh chỉ 2ms (G-to-G) kết hợp công nghệ G-sync Ultimate cho trải nghiệm game mượt mà, sống động nhất, kể cả game có nội dung HDR. Độ sáng tối ưu 1000nits, giúp nhìn rõ ngay cả những vùng tối nhất trong game. Thiết kế đèn Ambient light hình đôi cánh độc quyền trên Predator X35, tạo ra những dải ánh sáng phong cách đầy màu sắc, mang lại góc máy cực cá tính. Thiết kế chân đế kim loại cool ngầu cứng cáp tựa lớp áo giáp hầm hố, bắt mắt, với khả năng xoay, gập, điểu chỉnh độ cao và xoay linh hoạt. Phần mềm RGB Light Sense kết hợp với Lightstick, tạo ra các dải ánh sáng RGB chuyển động theo nhịp điệu nhạc hoặc âm lượng của game và phim, nhạc. Với 9 kiểu ánh sáng ngầu chất chơi. 2 loa tích hợp 4W cho âm thanh sống động, thiết kế cách điệu có khe phía sau lưng giúp tản nhiệt tốt','',100,4),
						  (N'Ghế Noble Chair Hero Series Real Leather - Black',25000000,N'Hero series là dòng ghế có kích thước lớn nhất trong các dòng Noblechair , sở hữu tay vịn mới, cơ sở chỗ ngồi rộng hơn và tựa lưng cao hơn, phù hợp hơn với nhiều đối tượng người sử dụng khác nhau. Với tính năng điều chỉnh độ phồng của lưng ghế thông qua núm vặn sẽ giúp người dùng có thể lựa chọn một tư thế ngồi thoải mái nhất cho riêng mình, tính năng này chỉ thường được tìm thấy ở những chiếc ghế trong những mẫu xe hơi cao cấp. Giờ đây Noble Chair Hero series đã có thêm phiên bản sử dụng chất liệu Da thật cao cấp tăng thêm vẻ sang trọng đẳng cấp và độ bền của sản phẩm.','',100,4)

INSERT INTO Combo VALUES(N'Latop Dell + RAM 8GB',29800000,''),
						(N'G-Station K303+ HDD + Case + Tản Khí',37100000,''),
						(N'Bàn phím + Màn hình',176000000,''),
						(N'Tai nghe + Ghế',33500000,''),
						(N'Laptop HP + Bàn phím',31000000,'')

INSERT INTO ComboSanPham VALUES (1,1,1,29000000),
								(1,8,1,800000),
								(2,4,1,35000000),
								(2,8,1,1000000),
								(2,9,1,600000),
								(2,10,1,500000),
								(3,11,1,6000000),
								(3,13,1,170000000),
								(4,12,1,8500000),
								(4,14,1,25000000),
								(5,2,1,25000000),
								(5,11,1,6000000)

INSERT INTO NhanVien VALUES (N'Hoàng Minh Triết','triet','e10adc3949ba59abbe56e057f20f883e'),
							(N'Lê Việt Hoàng','hoang','e10adc3949ba59abbe56e057f20f883e'),
							(N'Nguyễn Hữu Đức','duc','e10adc3949ba59abbe56e057f20f883e'),
							(N'Mạc Vĩ Hào','hao','e10adc3949ba59abbe56e057f20f883e'),
							(N'Phạm Minh Hiển','hien','e10adc3949ba59abbe56e057f20f883e')

INSERT INTO KhachHang VALUES (N'Hoàng Minh Tuyền','tuyen','e10adc3949ba59abbe56e057f20f883e',N'69/20 Quang Trung, TP.HCM'),
							 (N'Lê Văn Toàn','toan','e10adc3949ba59abbe56e057f20f883e',N'290 Nguyễn Văn Cừ, TP.HCM'),
							 (N'Nguyễn Hữu Huân','huan','e10adc3949ba59abbe56e057f20f883e',N'365/12/20 Lý Thường Kiệt, TP.HCM'),
							 (N'Mạc Vĩ Trung','trung','e10adc3949ba59abbe56e057f20f883e',N'200 Lê Đại Hành, TP.HCM'),
							 (N'Phạm Nhật Hạ','ha','e10adc3949ba59abbe56e057f20f883e',N'304/50 Lê Văn Thọ, TP.HCM')

INSERT INTO HoaDon VALUES ('2020-01-20 08:00:00',1,1),
						  ('2020-01-20 09:10:00',1,2),
						  ('2020-02-15 09:30:00',2,5),
						  ('2020-03-01 20:00:00',2,3),
						  ('2020-04-12 12:45:00',3,4),
						  ('2020-05-20 10:00:00',3,1),
						  ('2020-06-19 14:12:00',4,5),
						  ('2020-07-20 08:00:00',4,1),
						  ('2020-08-09 19:00:00',5,1),
						  ('2020-09-29 21:00:00',5,2)

INSERT INTO ChiTietHoaDon_SanPham VALUES(1,1,1,22000000),
										(2,2,1,25000000),
										(3,14,1,25000000),
										(4,13,1,170000000),
										(5,12,1,850000),
										(6,2,1,37100000),
										(7,3,1,176000000),
										(8,4,1,33500000),
										(9,1,1,29800000),
										(10,5,1,31000000)