﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;
using ToDoList.Services;
using ToDoList.ViewModel;

namespace ToDoList.Controllers
{
    public class SuperAdminController : Controller
    {
        private EmployeeService _service;

        public SuperAdminController()
        {
            _service = new EmployeeService();
        }
        // GET: SuperAdmin
        public ActionResult Index(string sortOrder, string searchString, string currentFilter, int pageIndex = 1)
        {
            ViewBag.currenSort = sortOrder;
            ViewBag.sortName = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.sortBirthDate = sortOrder == "Birthdate_desc" ? "Birthdate" : "Birthdate_desc";

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.currentFilter = searchString;

            if (Session["username"] != null && Session["role"].Equals("3"))
            {
                EmployeeIndexVM vm = _service.getAllAdmin(sortOrder, searchString, pageIndex);
                return View(vm);
            }
            return RedirectToAction("Index", "Account");

        }
        public ActionResult Create()
        {

            if (Session["username"] != null && Session["role"].Equals("3"))
            {
                return View();
            }
            return RedirectToAction("Index", "Account");

        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Name, Birthdate, Gender, Email, RoleId")]  Employee employee)
        {
            if (Session["username"] != null && Session["role"].Equals("3"))
            {
                if (ModelState.IsValid && !_service.isExistEmail(employee.Email, null))
                {
                    ViewBag.genderError = "";
                    ViewBag.roleError = "";
                    _service.createEmployee(employee);
                    return RedirectToAction("Index", "SuperAdmin");
                }
                if (_service.isExistEmail(employee.Email, null))
                {
                    ViewBag.errorMessage = "Email đã tồn tại";
                }
                if (employee.Gender.Equals("0"))
                {
                    ViewBag.genderError = "Vui lòng chọn giới tính";
                }
                else
                {
                    ViewBag.genderVal = employee.Gender;
                }
                if (employee.RoleId == 0)
                {
                    ViewBag.roleError = "Vui lòng chọn chức vụ";
                }
                else
                {
                    ViewBag.roleVal = employee.RoleId;
                }
                return View(employee);
            }
            return RedirectToAction("Index", "Account");

        }
        public ActionResult Edit(int? id)
        {
            if (Session["username"] == null)
            {
                return RedirectToAction("Index", "Account");
            }
            if (id == null)
            {
                return RedirectToAction("Index", "SuperAdmin");
            }
            Employee employee = _service.getById(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.genderVal = employee.Gender;
            ViewBag.roleVal = employee.RoleId;
            return View(employee);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id, Name, Birthdate, Gender, Email, RoleId")]  Employee employee)
        {
            if (ModelState.IsValid && !_service.isExistEmail(employee.Email, employee.Id))
            {
                _service.edit(employee);
                return RedirectToAction("Index");
            }
            if (_service.isExistEmail(employee.Email, employee.Id))
            {
                ViewBag.errorMessage = "Email đã tồn tại vui lòng nhập địa chỉ email khác";
            }
            ViewBag.genderVal = employee.Gender;
            ViewBag.roleVal = employee.RoleId;
            return View(employee);
        }
        public ActionResult Detail(int? id)
        {
            if (Session["username"] == null)
            {
                return RedirectToAction("Index", "Account");
            }
            if (id == null)
            {
                return RedirectToAction("Index", "SuperAdmin");
            }
            Employee employee = _service.getById(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }
    }
}