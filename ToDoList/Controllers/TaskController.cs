﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;
using ToDoList.Services;
using ToDoList.ViewModel;

namespace ToDoList.Controllers
{
    public class TaskController : Controller
    {
        private TaskService _service;
        public TaskController()
        {
            _service = new TaskService();
        }

        private void createViewBagForView()
        {
            List<TaskStatu> taskStatus = _service.GetAllTaskStatus();
            List<Employee> employee = _service.GetAllEmployee();

            SelectList listEmployee = new SelectList(employee, "Id", "Name", 0);
            SelectList listStatus = new SelectList(taskStatus, "Id", "StatusName", 1);
            ViewBag.listEmployee = listEmployee;
            ViewBag.listStatus = listStatus;
            
        }

        // GET: Task
        public ActionResult Index(string searchString, string currentFilter,string filterByTaskStatus, int pageIndex = 1)
        {

            if (Session["userId"] != null)
            {
                if (Session["role"].Equals("2"))
                {
                    int userId = Convert.ToInt32(Session["userId"]);
                    if (searchString != null)
                    {
                        pageIndex = 1;
                    }
                    else
                    {
                        searchString = currentFilter;
                    }
                    List<TaskStatu> listTaskStatus = _service.GetAllTaskStatus();
                    //ViewBag.listTaskStatus = 
                    if(String.IsNullOrEmpty(filterByTaskStatus))
                    {

                    }

                    ViewBag.currentFilter = searchString;
                    TaskIndexVM vm = _service.getAllTaskOfUser(searchString, pageIndex, userId);
                    return View(vm);
                }
                else
                {
                    if (searchString != null)
                    {
                        pageIndex = 1;
                    }
                    else
                    {
                        searchString = currentFilter;
                    }
                    ViewBag.currentFilter = searchString;
                    TaskIndexVM vm = _service.getAllTask(searchString, pageIndex);
                    return View(vm);
                }

            }
            return RedirectToAction("Index", "Account");
        }


        public ActionResult PublicTask(string searchString, string currentFilter, string filterByTaskStatus, int pageIndex = 1)
        {
            if(Session["userId"] == null || Session["role"].ToString().Equals("1") || Session["role"].ToString().Equals("3"))
            {
                return RedirectToAction("Index", "Account");
            }

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.currentFilter = searchString;
            TaskIndexVM vm = _service.getAllPublicTask(searchString, pageIndex);
            return View(vm);
        }

        [HttpPost]
        public void autoUpdateTask()
        {
            _service.autoUpdateTask();
        }

        public ActionResult Create()
        {
            if (Session["userId"] != null)
            {
                createViewBagForView();

                return View(new TaskCreateVM());

            }
            return RedirectToAction("Index", "Account");
        }

        private bool dateValid(DateTime start,DateTime end)
        {
            if (DateTime.Compare(start, end) < 0)
                return true;
            return false;
        }

        [HttpPost]
        public ActionResult Create(TaskCreateVM TaskCreateVM)
        {
            if (Session["userId"] != null)
            {
                //Valid input
                bool flag = true;
                DateTime defaultDate = new DateTime(1,1,1);
                DateTime now = DateTime.Now;

                if(string.IsNullOrEmpty(TaskCreateVM.Title))
                {
                    ViewBag.errorTitle = "Vui lòng nhập tiêu đề";
                    flag = false;
                }
                if(DateTime.Compare(TaskCreateVM.StartDate,defaultDate) == 0)
                {
                    ViewBag.errorStartDate = "Vui lòng chọn thời điểm bắt đầu";
                    flag = false;
                }
                if (DateTime.Compare(TaskCreateVM.EndDate, defaultDate) == 0)
                {
                    ViewBag.errorEndDate = "Vui lòng chọn thời điểm kết thúc";
                    flag = false;
                }
                if(TaskCreateVM.StartDate != null && TaskCreateVM.EndDate != null && ViewBag.errorEndDate == null )
                {
                    if (!dateValid(TaskCreateVM.StartDate, TaskCreateVM.EndDate))
                    {
                        ViewBag.ErrorEndDate = "Thời điểm bắt đầu phải lớn hơn thời điểm kết thúc";
                        flag = false;
                    }
                }
                if( DateTime.Compare(now,TaskCreateVM.EndDate)  > 0 && ViewBag.errorEndDate == null)
                {
                    ViewBag.errorEndDate = "Thời điểm kết thúc phải lớn hơn hiện tại";
                    flag = false;
                }
                if (TaskCreateVM.EmployeeIsAssigned == null && Session["role"].ToString().Equals("1"))
                {
                    ViewBag.ErrorEmployeeIsAssigned = "Vui lòng phân công";
                    flag = false;
                }
                if (TaskCreateVM.File != null && TaskCreateVM.File.Count(file => file != null) != 0)
                {
                    foreach (HttpPostedFileBase item in TaskCreateVM.File)
                    {
                        if (item.ContentLength >= 4 * 1024 * 1024)
                        {
                            ViewBag.fileError = "Dung lượng tệp đính kèm không được lớn hơn 4mb";
                            flag = false;
                            break;
                        }
                    }
                }

                if (flag == false)
                {
                    createViewBagForView();
                    return View(TaskCreateVM);
                }


                else if(ModelState.IsValid )
                {
                    //Add Task
                    Task task = new Task();
                    task.EndDate = TaskCreateVM.EndDate;
                    task.StartDate = TaskCreateVM.StartDate;
                    task.Scope = TaskCreateVM.Scope;
                    task.Title = TaskCreateVM.Title;
                    task.StatusId = TaskCreateVM.StatusId;

                    _service.Add(task);

                    if(Session["role"].ToString().Equals("1"))
                    {
                        //assign admin first to db
                        Assignment admin = new Assignment();
                        admin.EmployeeId = ParseSessionUserIdToInt();
                        admin.TaskId = task.Id;
                        _service.AddAssignment(admin);

                        foreach (int employeeId in TaskCreateVM.EmployeeIsAssigned)
                        {
                            Assignment a = new Assignment();
                            a.EmployeeId = employeeId;
                            a.TaskId = task.Id;
                            _service.AddAssignment(a);
                        }

                    }
                    else
                    {
                        Assignment a = new Assignment();
                        a.EmployeeId = ParseSessionUserIdToInt();
                        a.TaskId = task.Id;

                        _service.AddAssignment(a);
                    }

                    //upload file
                    if (TaskCreateVM.File != null && TaskCreateVM.File.Count(file => file != null) != 0)
                    {
                        foreach(HttpPostedFileBase file in TaskCreateVM.File)
                        {
                            //Use Namespace called :  System.IO  
                            string FileName = Path.GetFileNameWithoutExtension(file.FileName).Trim();

                            //To Get File Extension  
                            string FileExtension = Path.GetExtension(file.FileName);

                            //Add Current Date To Attached File Name  

                            //Get Upload path from Web.Config file AppSettings.  
                            string UploadPath = Server.MapPath(@"\File\");

                            //string path = UploadPath + FileName + FileExtension;
                            //Its Create complete path to store in server.  
                            //FileName += FileExtension;
                            String fullName = FileName + FileExtension;
                            if (_service.isFileNameDuplicate(fullName))
                            {
                                fullName = _service.createFileName(1, _service.GetAllFileManager(), FileName, FileExtension, fullName);
                            }

                            //Add to file manager
                            FileManager fileManager = new FileManager();
                            fileManager.C_Path = fullName;
                            fileManager.TaskId = task.Id;

                            _service.AddFileManager(fileManager);

                            //To copy and save file into server.  
                            file.SaveAs(UploadPath + fullName);

                        }

                    }

                    //Write Log
                    TaskLog tl = new TaskLog();
                    tl.TaskId = task.Id;
                    tl.Content = "Đã khởi tạo công việc";
                    tl.C_TimeStamp = DateTime.Now;
                    tl.EmployeeId = ParseSessionUserIdToInt();
                    _service.AddTaskLog(tl);


                    return RedirectToAction("Detail", "Task", new { id = task.Id } );

                }

            }
            return RedirectToAction("Index", "Account");
        }

        public ActionResult Edit(int? id)
        {
            if (Session["userId"] == null || id == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                int userId = Int32.Parse(Session["userId"].ToString());
                int role = Int32.Parse(Session["role"].ToString());
                if(role != 1)
                {
                    if (!_service.isUserAssignForTask(userId, id.GetValueOrDefault()))
                    {
                        return RedirectToAction("PublicTask", "Task");
                    }
                }
                
                //TaskCreateVM vm = _service.getTaskCreateVM(id);
                TaskEditVM vm = _service.getTaskEditVM(id);

                List<Employee> listEmployee = _service.GetAllEmployee();
                List<Employee> EmployeeIsAssigned = _service.GetEmployeeByTaskId(id);

                MultiSelectList MTEmployeeIsAssigned = new MultiSelectList(listEmployee, "Id", "Name", _service.ListEmployeeIdIsAssign(EmployeeIsAssigned) );
                ViewBag.MTEmployeeIsAssigned = MTEmployeeIsAssigned;

                ViewBag.Scope = vm.Scope;

                List<TaskStatu> taskStatus = _service.GetAllTaskStatus();
                SelectList listStatus = new SelectList(taskStatus, "Id", "StatusName");
                ViewBag.listStatus = listStatus;

                return View(vm);
            }
        }

        [HttpPost]
        public ActionResult Edit(TaskEditVM TaskEditVM)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                bool flag = true;
                DateTime defaultDate = new DateTime(1, 1, 1);
                DateTime now = DateTime.Now;

                if (string.IsNullOrEmpty(TaskEditVM.Title))
                {
                    ViewBag.errorTitle = "Vui lòng nhập tiêu đề";
                    flag = false;
                }
                if (DateTime.Compare(TaskEditVM.StartDate, defaultDate) == 0)
                {
                    ViewBag.errorStartDate = "Vui lòng chọn thời điểm bắt đầu";
                    flag = false;
                }
                if (DateTime.Compare(TaskEditVM.EndDate, defaultDate) == 0)
                {
                    ViewBag.errorEndDate = "Vui lòng chọn thời điểm kết thúc";
                    flag = false;
                }
                if (TaskEditVM.StartDate != null && TaskEditVM.EndDate != null)
                {
                    if (!dateValid(TaskEditVM.StartDate, TaskEditVM.EndDate))
                    {
                        ViewBag.ErrorEndDate = "Thời điểm bắt đầu phải lớn hơn thời điểm kết thúc";
                        flag = false;
                    }
                }
                //if (DateTime.Compare(now, TaskCreateVM.EndDate) > 0)
                //{
                //    ViewBag.errorEndDate = "Thời điểm kết thúc phải lớn hơn hiện tại";
                //    flag = false; 
                //}
                if (TaskEditVM.EmployeeIsAssigned == null && Session["role"].ToString().Equals("1"))
                {
                    ViewBag.ErrorEmployeeIsAssigned = "Vui lòng phân công";
                    flag = false;
                }
                if (TaskEditVM.File != null && TaskEditVM.File.Count(file => file != null) != 0)
                {
                    foreach (HttpPostedFileBase item in TaskEditVM.File)
                    {
                        if (item.ContentLength >= 4 * 1024 * 1024)
                        {
                            ViewBag.fileError = "Dung lượng tệp đính kèm không được lớn hơn 4mb";
                            flag = false;
                            break;
                        }
                    }
                }

                if (flag == false)
                {
                    TaskEditVM vm = _service.getTaskEditVM(TaskEditVM.Id);

                    List<Employee> listEmployee = _service.GetAllEmployee();
                    List<Employee> EmployeeIsAssigned = _service.GetEmployeeByTaskId(TaskEditVM.Id);

                    MultiSelectList MTEmployeeIsAssigned = new MultiSelectList(listEmployee, "Id", "Name", _service.ListEmployeeIdIsAssign(EmployeeIsAssigned));
                    ViewBag.MTEmployeeIsAssigned = MTEmployeeIsAssigned;

                    ViewBag.Scope = vm.Scope;

                    List<TaskStatu> taskStatus = _service.GetAllTaskStatus();
                    SelectList listStatus = new SelectList(taskStatus, "Id", "StatusName");
                    ViewBag.listStatus = listStatus;
                    return View(vm);

                }

                else
                {
                    Task t = new Task();
                    t.Id = TaskEditVM.Id;
                    t.Scope = TaskEditVM.Scope;
                    t.StartDate = TaskEditVM.StartDate;
                    t.EndDate = TaskEditVM.EndDate;
                    t.StatusId = TaskEditVM.StatusId;
                    t.Title = TaskEditVM.Title;
                    // TH1: End < Now và stt = Đang làm
                    if (DateTime.Compare(now, t.EndDate) > 0 && t.StatusId == 1)
                    {
                        t.StatusId = 3; //Trễ hạn
                    }

                    // TH2: End > Now và stt = TRẽ hạn
                    else if (DateTime.Compare(now, t.EndDate) < 0 && t.StatusId == 3)
                    {
                        t.StatusId = 1; // Đang làm
                    }

                    //Save new name file to writelog
                    List<string> listNewNameFile = new List<string>();
                    //upload file
                    if (TaskEditVM.File != null && TaskEditVM.File.Count(file => file != null) != 0)
                    {
                        foreach (HttpPostedFileBase file in TaskEditVM.File)
                        {
                            
                            //Use Namespace called :  System.IO  
                            string FileName = Path.GetFileNameWithoutExtension(file.FileName).Trim();

                            //To Get File Extension  
                            string FileExtension = Path.GetExtension(file.FileName);

                            //Add Current Date To Attached File Name  

                            //Get Upload path from Web.Config file AppSettings.  
                            string UploadPath = Server.MapPath(@"\File\");

                            //string path = UploadPath + FileName + FileExtension;
                            //Its Create complete path to store in server.  
                            //FileName += FileExtension;
                            String fullName = FileName + FileExtension;
                            if (_service.isFileNameDuplicate(fullName))
                            {
                                fullName = _service.createFileName(1, _service.GetAllFileManager(), FileName, FileExtension, fullName);
                            }

                            //Add to file manager
                            FileManager fileManager = new FileManager();
                            fileManager.C_Path = fullName;
                            fileManager.TaskId = t.Id;

                            //add new filename for list
                            listNewNameFile.Add(fullName);
                            _service.AddFileManager(fileManager);

                            //To copy and save file into server.  
                            file.SaveAs(UploadPath + fullName);

                        }

                    }

                    //WriteLog
                    string log = _service.writeTaskLogWhenTitleChange(t.Id, ParseSessionRoleToInt(), ParseSessionUserIdToInt(), t.Title);
                    log += _service.writeTaskLogWhenStartDateChange(t.Id, ParseSessionRoleToInt(), ParseSessionUserIdToInt(), t.StartDate);
                    log += _service.writeTaskLogWhenEndDateChange(t.Id, ParseSessionRoleToInt(), ParseSessionUserIdToInt(), t.EndDate);
                    log += _service.writeTaskLogWhenStatusChange(t.Id, ParseSessionRoleToInt(), ParseSessionUserIdToInt(), t.StatusId);
                    log += _service.writeTaskLogWhenEditTaskScope(t.Id, ParseSessionRoleToInt(), ParseSessionUserIdToInt(),t.Scope);
                    if (Session["role"].ToString().Equals("1"))
                    {
                        log += _service.writeTaskLogAssignChange(t.Id, ParseSessionRoleToInt(), ParseSessionUserIdToInt(), TaskEditVM.EmployeeIsAssigned);
                    }
                    if (TaskEditVM.File != null && TaskEditVM.File.Count(file => file != null) != 0)
                    {
                        log += _service.writeTaskLogWhenAddFile(t.Id, ParseSessionRoleToInt(), ParseSessionUserIdToInt(), listNewNameFile);
                    }

                    //nếu có thay đổi thì mới viết  log
                    if(!string.IsNullOrEmpty(log))
                    {
                        TaskLog tl = new TaskLog();
                        tl.Content = log;
                        tl.EmployeeId = ParseSessionUserIdToInt();
                        tl.C_TimeStamp = DateTime.Now;
                        tl.TaskId = t.Id;

                        //Add Task Log
                        _service.AddTaskLog(tl);
                    }
                    

                    if(TaskEditVM.EmployeeIsAssigned != null)
                    {
                        //Delete all assign have task id except admin
                        _service.RemoveAssignByTaskId(t.Id); // ủa mà sao không remove hêt bà nó luôn đi rồi add lại 
                        //TaskEditVM.EmployeeIsAssigned cái lít này là cái viewbag mình tạo, mà viewbag mình tạo đâu có lãnh đạo trong đó uh đúng rồi sẻvice
                        //Add new Assign
                        // ủa cái hàm trên là viêt ở đâu vậy ông
                        // thì coi như là xóa hêt nhân viên assign cho cái task đó luôn kể cả admin rồi làm lại y như create 
                        foreach (int employeeId in TaskEditVM.EmployeeIsAssigned)
                        {
                            Assignment a = new Assignment();
                            a.EmployeeId = employeeId;
                            a.TaskId = t.Id;
                            //nhưng cái lít này ko có lãnh đạo trong đó chỉ cần lấy id của lãnh đạo thoi phải không là thằng đang sửa á session["userid"]
                            //vậy ông nghxi gì sẽ xảy ra khi lãnh đạo 1 sửa task của lãnh đạo 2
                            // căng haowcj mà để dễ thì bên kia có cái hidden field nào không lưu id của lãnh đạo đó xuống vào đấy
                            // ông muốn xóa hết để đảm bảo cái thứ tự trong db đúng ko ?uh 
                            // làm thì cũng đc uh nhưng mà cái vụ lãnh đạo một sửa công việc của lãnh đạo hai hơi căng nhỉ tính ra mình nên làm cái vụ lãnh đạo đó có
                            // tham gia công việc đó thìm mới được sửa
                            // ban đầu tui cũng nghĩ nên để lãnh đạo tham gia
                            // kiểu như giám sát, thực tế cũng vậy mà
                            // mà check cái vụ tham gia đó dễ không nhỉ // ê nói hào cái
                            _service.AddAssignment(a);
                        }
                    }
                   

                    //update Task
                    _service.Update(t);

                    return RedirectToAction("Detail", "Task", new { id = t.Id } );
                }
            }
        }

        //Global.asax
        private void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            var httpException = ex as HttpException ?? ex.InnerException as HttpException;
            if (httpException == null) return;

            if (((System.Web.HttpException)httpException.InnerException).WebEventCode == System.Web.Management.WebEventCodes.RuntimeErrorPostTooLarge)
            {
                //handle the error
                Response.Write("Too big a file, dude"); //for example
            }
        }

        private int ParseSessionRoleToInt()
        {
            return Int32.Parse(Session["role"].ToString());
        }

        private int ParseSessionUserIdToInt()
        {
            return Int32.Parse(Session["userId"].ToString());
        }

        [HttpPost]
        public ActionResult updateScopeOfTask(string taskId, bool scope)
        {
            if (Session["userId"] != null)
            {
                _service.updateScopeOfTask(Int32.Parse(taskId), scope);
                _service.writeTaskLogWhenTaskScopeChange(Int32.Parse(taskId), Convert.ToString(Session["role"]), Convert.ToString(Session["userId"]), scope);
                string message = scope ? "Đã cập nhật trạng thái thành public " : "Đã cập nhật trạng thái thành private";
                return Json(new { Message = message });
            }
            return RedirectToAction("Index", "Account");
        }

        [HttpPost]
        public ActionResult deleteFile(int fileId, int taskId)
            {
            if (Session["userId"] != null)
            {
                _service.writeTaskLogWhenDeleteFile(fileId, ParseSessionRoleToInt(), ParseSessionUserIdToInt(),taskId);
                _service.DeleteFileManagerById(fileId);

                TaskEditVM vm = _service.getTaskEditVM(taskId);

                List<Employee> listEmployee = _service.GetAllEmployee();
                List<Employee> EmployeeIsAssigned = _service.GetEmployeeByTaskId(taskId);

                MultiSelectList MTEmployeeIsAssigned = new MultiSelectList(listEmployee, "Id", "Name", _service.ListEmployeeIdIsAssign(EmployeeIsAssigned));
                ViewBag.MTEmployeeIsAssigned = MTEmployeeIsAssigned;

                ViewBag.Scope = vm.Scope;

                List<TaskStatu> taskStatus = _service.GetAllTaskStatus();
                SelectList listStatus = new SelectList(taskStatus, "Id", "StatusName");
                ViewBag.listStatus = listStatus;

                return PartialView("_TableFilePartial", vm);
            }
            return RedirectToAction("Index", "Account");
           
        }

        public ActionResult Detail(int? id) // id : taskId
        {
            if (Session["username"] == null || id == null)
            {
                return RedirectToAction("Index", "Account");
            }
            DetailTaskVM vm = _service.loadTaskDetail(id);
            if (vm == null)
            {
                return RedirectToAction("Index", "Task");
            }
            return View(vm);
        }

        //post comment and reload page
        [HttpPost]
        public ActionResult postComment(int taskId, string content)
        {
            Comment comment = new Comment();
            comment.Content = content;
            comment.EmployeeId = Int32.Parse(Session["userId"].ToString());
            comment.TaskId = taskId;
            comment.C_TimeStamp = DateTime.Now;
            _service.addComment(comment);
            DetailTaskVM vm = _service.loadTaskDetail(taskId);
            return PartialView("_CommentPartial", vm);
        }

        public FileResult DownloadFile(string fileName)
        {
            string path = Server.MapPath(@"\File\") + fileName;
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}