﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;
using ToDoList.Services;
using ToDoList.ViewModel;

namespace ToDoList.Controllers
{
    public class EmployeeController : Controller
    {
        private EmployeeService _service;

        public EmployeeController()
        {
            _service = new EmployeeService();
        }

        public ActionResult Index(string sortOrder,string searchString, string currentFilter,  int pageIndex = 1)
        {
            ViewBag.currenSort = sortOrder;
            ViewBag.sortName = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";
            ViewBag.sortBirthDate = sortOrder == "Birthdate_desc" ? "Birthdate" : "Birthdate_desc";

            if (searchString != null)
            {
                pageIndex = 1;
            } else
            {
                searchString = currentFilter;
            }

            ViewBag.currentFilter = searchString;

            if (Session["username"] != null && Session["role"].Equals("1"))
            {
                EmployeeIndexVM vm = _service.getAllEmployee(sortOrder, searchString, pageIndex);
                return View(vm);
            }
                return RedirectToAction("Index", "Account");

        }

        public ActionResult Create()
        {

            if (Session["username"] != null && Session["role"].Equals("1"))
            {
                return View();
            }
            return RedirectToAction("Index", "Account");

        }

        [HttpPost]
        public ActionResult Create([Bind(Include ="Name, Birthdate, Gender, Email, RoleId")]  Employee employee)
        {
            if (Session["username"] != null && Session["role"].Equals("1"))
            {
                if (ModelState.IsValid && !_service.isExistEmail(employee.Email, null))
                {
                    ViewBag.genderError = "";
                    ViewBag.roleError = "";
                    _service.createEmployee(employee);
                    return RedirectToAction("Index", "Employee");
                }
                if (_service.isExistEmail(employee.Email, null))
                {
                    ViewBag.errorMessage = "Email đã tồn tại";
                }
                if (employee.Gender.Equals("0"))
                {
                    ViewBag.genderError = "Vui lòng chọn giới tính";
                } else
                {
                    ViewBag.genderVal = employee.Gender;
                }
                if(employee.RoleId == 0 )
                {
                    ViewBag.roleError = "Vui lòng chọn chức vụ";
                } else
                {
                    ViewBag.roleVal = employee.RoleId;
                }
                return View(employee);
            }
            return RedirectToAction("Index", "Account");
            
        }

        public ActionResult Edit(int? id)
        {
            if(Session["username"] == null)
            {
                return RedirectToAction("Index", "Account");
            }
            if(id == null)
            {
                return RedirectToAction("Index", "Employee");
            }
            Employee employee = _service.getById(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.genderVal = employee.Gender;
            ViewBag.roleVal = employee.RoleId;
            return View(employee);
        }

        [HttpPost] 
        public ActionResult Edit([Bind(Include = "Id, Name, Birthdate, Gender, Email, RoleId")]  Employee employee)
        {
            //&& !_service.isExistEmail(employee.Email, employee.Id)
            if (ModelState.IsValid)
            {
                if (_service.isExistEmail(employee.Email, employee.Id))
                {
                    ViewBag.errorMessage = "Email đã tồn tại";
                    return View(employee);
                } else
                {
                    _service.edit(employee);
                    return RedirectToAction("Detail", "Employee", new { id = employee.Id });
                }
               
            }
           
            if (employee.Gender.Equals("0"))
            {
                ViewBag.genderError = "Vui lòng chọn giới tính";
            }
            else
            {
                ViewBag.genderVal = employee.Gender;
            }
            if (employee.RoleId == 0)
            {
                ViewBag.roleError = "Vui lòng chọn chức vụ";
            }
            else
            {
                ViewBag.roleVal = employee.RoleId;
            }
            ViewBag.genderVal = employee.Gender;
            ViewBag.roleVal = employee.RoleId;
            return View(employee);
        }
        [HttpPost]
        public ActionResult lockAndUnlockUser(String userId, bool status)
        {
            if (Session["username"] != null && (Session["role"].Equals("1") || Session["role"].Equals("3")))
            {
                _service.lockAndUnlockUser(Int32.Parse(userId), status);
                string message = !status ? "Đã khóa người dùng" : "Đã mở khóa người dùng";
                return Json(new { Message = message });
            }
            return RedirectToAction("Index", "Account");

        }
        public ActionResult Detail(int? id)
        {
            if (Session["username"] == null || id == 0)
            {
                return RedirectToAction("Index", "Account");
            }
            Employee employee = _service.getById(id);
            TaskService taskService = new TaskService();
            TaskIndexVM taskIndexVM = taskService.getAllTaskOfUser("", 1, id.GetValueOrDefault());
            if (employee == null)
            {
                return HttpNotFound();
            }
            var tuple = new Tuple<Employee, TaskIndexVM>(employee, taskIndexVM);
            return View(tuple);
        }
    }
}