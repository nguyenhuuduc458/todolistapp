﻿using System;
using System.Web.Mvc;
using System.Web.SessionState;
using ToDoList.Models;
using ToDoList.Services;

namespace ToDoList.Controllers
{
    [SessionState(SessionStateBehavior.Default)]
    public class AccountController : Controller
    {
        private EmployeeService _service;
        public AccountController()
        {
            _service = new EmployeeService();
        }


        // GET: Account
        public ActionResult Index()
        {
            if(Session["username"] != null)
            {
                return RedirectToAction("Index", "Home");

            }
            return View();
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "Email, C_Password")] Employee user)
        {
            string username = user.Email;
            string password = user.C_Password;
            Employee employee = _service.Login(username, password);
            if (employee == null)
            {
                @ViewData["Error"] = "Tên đăng nhập hoặc mật khẩu không hợp lệ";
                return View();
            }
            else
            {
               if(employee.IsActive == true)
                {
                    Session["name"] = employee.Name;
                    Session["username"] = employee.Email;
                    Session["role"] = Convert.ToString(employee.RoleId);
                    Session["userId"] = Convert.ToString(employee.Id);

                    return RedirectToAction("Index", "Home"); 
                } else
                {
                    @ViewData["Error"] = "Tài khoản đã bị khóa";
                    return View();
                }
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Account");
        }
    }
}