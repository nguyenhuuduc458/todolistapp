﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoList.Services;
using ToDoList.ViewModel;

namespace ToDoList.Controllers
{
    public class StatisticalController : Controller
    {
        private StatisticalService _service;
        private TaskService _taskService;
        public StatisticalController()
        {
            _service = new StatisticalService();
            _taskService = new TaskService();
        }
        // GET: Statistical
        public ActionResult Index()
        {
            if (Session["username"] == null || Session["role"].Equals("2"))
            {
                return RedirectToAction("Index", "Account");
            }
            return View();
        }

        [HttpGet]
        public ActionResult loadStatiscalTaskByStatus(DateTime startDate, DateTime endDate,  int statusId, int pageIndex = 1)
        {
            if (Session["username"] == null || Session["role"].Equals("2"))
            {
                return RedirectToAction("Index", "Account");
            }
            StatisticalIndexVM vm = null;
            if (statusId == 0)
            {
                vm = _service.loadDefaultStatistical(startDate, endDate, pageIndex);
            }
            else
            {
                vm = _service.loadStatiscalTaskByStatus(startDate, endDate, pageIndex, statusId);
            }

            return PartialView("StatisticalTaskByStatus", vm);
        }

        [HttpGet]
        public ActionResult loadStatiscalExpiredTask(DateTime startDate, DateTime endDate, int pageIndex = 1)
        {
            if (Session["username"] == null || Session["role"].Equals("2"))
            {
                return RedirectToAction("Index", "Account");
            }
            StatisticalIndexVM vm = _service.loadStatiscalExpiredTask(startDate, endDate, pageIndex);
            return PartialView("StatiscalExpiredTask", vm);
        }

        [HttpGet]
        public ActionResult loadStatiscalExpiredTaskDetail(DateTime startDate, DateTime endDate, int userId, int pageIndex = 1)
        {
            if (Session["username"] == null || Session["role"].Equals("2"))
            {
                return RedirectToAction("Index", "Account");
            }
            TaskIndexVM vm = _service.loadAllTaskOfUser(startDate, endDate, userId, pageIndex);
            return PartialView("_ModalView", vm);
        }

        [HttpPost]
        public ActionResult initStatistical(DateTime startDate, DateTime endDate, int pageIndex = 1)
        {
            if (Session["username"] == null || Session["role"].Equals("2"))
            {
                return RedirectToAction("Index", "Account");
            }
            StatisticalIndexVM vm = _service.loadDefaultStatistical(startDate, endDate, pageIndex);
            return PartialView("StatisticalTaskByStatus", vm);
        }
    }


}