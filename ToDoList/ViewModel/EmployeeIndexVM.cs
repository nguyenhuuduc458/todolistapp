using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoList.Models;
using ToDoList.Services;

namespace ToDoList.ViewModel
{
    public class EmployeeIndexVM
    {
        public PaginatedList<Employee> Employees { get; set; }
    }
}
