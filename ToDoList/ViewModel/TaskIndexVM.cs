﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoList.Models;
using ToDoList.Services;

namespace ToDoList.ViewModel
{
    public class TaskIndexVM
    {
        public PaginatedList<Task> Tasks { get; set; }
    }
}