﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoList.Models;
using ToDoList.Services;

namespace ToDoList.ViewModel
{
    public class StatisticalIndexVM
    {
        public PaginatedList<StatiscalTaskByStatusModel> statisticalTasks { get; set; }
        public PaginatedList<StatiscalExpiredTaskModel> statisticalExpiredTasks { get; set; }
    }
}