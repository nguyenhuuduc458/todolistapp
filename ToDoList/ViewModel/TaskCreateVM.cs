﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;

namespace ToDoList.ViewModel
{
    public class TaskCreateVM : Task
    {
        public List<int> EmployeeIsAssigned { get; set; }
        public HttpPostedFileBase[] File { get; set; }
    }
}