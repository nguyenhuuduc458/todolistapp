﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ToDoList.Models;
using ToDoList.Services;

namespace ToDoList.ViewModel
{
    public class DetailTaskVM
    {

        public Task task { get; set; }
        public IEnumerable<CommentModel> Comments { get; set; }
        public List<TaskLogModel> TaskLogs { get; set; } 
        public List<EmployeeAssignWorkModel> EmployeeAssign { get; set; }
        public List<FileManager> Files { get; set;}
    }
}