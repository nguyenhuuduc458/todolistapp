﻿-- Drop database if exists
USE master
IF EXISTS ( SELECT * FROM sys.databases WHERE name = 'ToDoList' )
DROP DATABASE ToDoList

-- Create database
CREATE DATABASE ToDoList
USE ToDoList

CREATE TABLE TaskStatus
(
	Id INT IDENTITY(1,1),
	StatusName NVARCHAR(50) NOT NULL,
	PRIMARY KEY (Id)
)

CREATE TABLE Role
(
	Id INT IDENTITY (1,1),
	RoleName NVARCHAR(50) NOT NULL,
	_Description NVARCHAR(255),
	PRIMARY KEY (Id)
)

CREATE TABLE Employee
(
	Id INT IDENTITY(1,1),
	Name NVARCHAR(50) NOT NULL,
	BirthDate DATE NOT NULL,
	Gender NVARCHAR(4) NOT NULL,
	Email VARCHAR(255) NOT NULL,
	_Password VARCHAR(255) NOT NULL,
	IsActive BIT DEFAULT 0,			-- 0: not active, 1: active
	RoleId INT NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK_EMPLOYEE_ROLE FOREIGN KEY(RoleId) References dbo.Role(Id)
)

CREATE TABLE Task
(
	Id INT IDENTITY(1,1),
	Title NVARCHAR(255) NOT NULL,
	StartDate DATETIME NOT NULL,
	EndDate DATETIME NOT NULL,
	Scope BIT NOT NULL,				-- 0: private, 1: public
	StatusId INT NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK_TASK_TASKSTATUS FOREIGN KEY(StatusId) REFERENCES dbo.TaskStatus(Id) 
)

CREATE TABLE Comment
(
	Id INT IDENTITY(1,1),
	Content NVARCHAR(255) NOT NULL,
	EmployeeId INT NOT NULL,
	TaskId INT NOT NULL,
	_TimeStamp DATETIME NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK_COMMENT_EMPLOYEE FOREIGN KEY(EmployeeId) REFERENCES dbo.Employee(Id),
	CONSTRAINT FK_COMMENT_TASK FOREIGN KEY(TaskId) REFERENCES dbo.Task(Id)
)

CREATE TABLE Assignment
(
	Id INT IDENTITY(1, 1),
	EmployeeId INT NOT NULL,
	TaskId INT NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK_ASSIGNMENT_EMPLOYEE FOREIGN KEY(EmployeeId) REFERENCES dbo.Employee(Id),
	CONSTRAINT FK_ASSIGNMENT_TASK FOREIGN KEY(TaskId) REFERENCES dbo.Task(Id),
)

CREATE TABLE FileManager
(
	Id INT IDENTITY(1, 1),
	_Path NVARCHAR(255) NOT NULL,
	TaskId INT NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK_FILEMANAGER_TASK FOREIGN KEY(TaskId) REFERENCES dbo.Task(Id)
)

CREATE TABLE TaskLog
(
	Id INT IDENTITY(1, 1),
	EmployeeId INT NOT NULL,
	TaskId INT NOT NULL,
	Content NVARCHAR(MAX) NOT NULL,
	_TimeStamp DATETIME NOT NULL,
	PRIMARY KEY (Id),
	CONSTRAINT FK_TASKLOG_EMPLOYEE FOREIGN KEY(EmployeeId) REFERENCES dbo.Employee(Id),
	CONSTRAINT FK_TASKLOG_TASK FOREIGN KEY(TaskId) REFERENCES dbo.Task(Id)
)

/* INSERT DATAS */
--TABLE TaskStatus
DELETE FROM dbo.TaskStatus
DBCC CHECKIDENT ('TaskStatus', RESEED, 1)

INSERT INTO dbo.TaskStatus
		( StatusName )
VALUES  
		( N'Đang làm'),
		( N'Đã xong'),
		( N'Trễ hạn')

--TABLE Role
DELETE FROM dbo.Role
DBCC CHECKIDENT ('Role', RESEED, 1)

INSERT INTO dbo.Role
        ( RoleName, [_Description] )
VALUES  
        ( N'Lãnh đạo', N'Quản lý nhân viên, xem danh sách công việc'),
		( N'Nhân viên', N'Lập danh sách các công việc, đánh dấu công việc hoặc chỉnh sửa danh sách công việc'),
		( N'Super admin', N'Quản lý các lãnh đạo')

--TABLE Employee
DELETE FROM dbo.Employee
DBCC CHECKIDENT ('Employee', RESEED, 1)

INSERT INTO dbo.Employee 
VALUES
	(N'Nguyễn Hữu Đức', '10/04/1989', 'Nam', 'nguyenhuuduc458@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Phạm Minh Hiển', '12/06/1989', 'Nam', 'pmhien2703@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Mạc Vĩ Hào', '10/06/1989', 'Nam', 'mvhix9@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Hoàng Minh Triết', '10/07/1995', 'Nam', 'hoangminhtriet@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Lê Việt Hoàng', '10/08/1999', 'Nam', 'leviethoang@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Phạm Tuyết Quỳnh Như', '06/25/1999', N'Nữ', 'nguyenquynhnhu@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Lê Phương Thảo', '03/25/1999', N'Nữ', 'lephuongthao@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Nguyễn Xuân Mai', '08/07/1999', N'Nữ', 'nguyenxuanmai@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Nguyễn Trúc Quỳnh', '05/06/1999', N'Nữ', 'nguyentrucquynh@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Phạm Trúc Quỳnh', '04/02/1999', N'Nữ', 'phamtrucquynh@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 2),
	(N'Nguyễn Văn Bằng', '10/04/1999', 'Nam', 'root@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 1),
	(N'Quản trị viên cấp cao', '10/04/1999', 'Nam', 'root2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 3),
	(N'Thái Bảo Nam', '10/04/1997', 'Nam', 'rootV1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 1),
	(N'Nguyễn Phi Phụng', '10/04/1996', N'Nữ', 'rootV2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 1),
	(N'Nguyễn Văn Tĩnh', '10/04/1995', 'Nam', 'rootV3@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', 1)

--TABLE TASK
DELETE FROM dbo.Task
DBCC CHECKIDENT ('Task', RESEED, 1)

INSERT INTO dbo.Task
VALUES 
	(N'Cập nhật database', '10/04/2020 08:30:00 AM', '10/16/2020 08:30:00 AM', 0,1),
	(N'Sửa báo cáo thống kê', '12/04/2020 08:30:00 AM', '12/07/2020 08:30:00 AM', 0,1),
	(N'Thống kê hàng tồn', '12/04/2020 08:30:00', '12/30/2020 08:30:00', 0,1),
	(N'Lập báo cáo nhập hàng','12/04/2020 08:30:00', '12/25/2020 08:30:00', 0,1),
	(N'Lập báo cáo lương nhân viên','12/04/2020 08:30:00', '12/21/2020 08:30:00', 0,1)

--TABLE ASSIGNMENT
DELETE FROM dbo.Assignment
DBCC CHECKIDENT ('Assignment', RESEED, 1)

INSERT INTO dbo.Assignment
VALUES 
	(1, 1),
	(2, 1),
	(4, 2),
	(3, 2),
	(5, 2),
	(6, 3),
	(7, 3),
	(8, 4),
	(9, 4),
	(10, 5)


--TABLE TASKLOG
DELETE FROM dbo.TaskLog
DBCC CHECKIDENT ('TaskLog', RESEED, 1)

INSERT INTO dbo.TaskLog
VALUES 

	(1, 1, N'Khởi tạo công việc','2020-10-04 08:30:00.000'),
	(4, 2, N'Khởi tạo công việc','2020-12-04 08:32:00.000'),
	(6, 3, N'Khởi tạo công việc','2020-12-04 08:35:00.000'),
	(8, 4, N'Khởi tạo công việc','2020-12-04 08:37:00.000'),
	(10, 5, N'Khởi tạo công việ','2020-12-04 08:39:00.000')

--TABLE COMMENT
DELETE FROM dbo.Comment
DBCC CHECKIDENT ('Comment', RESEED, 1)

--TABLE FileManagemer
DELETE FROM dbo.FileManager
DBCC CHECKIDENT ('FileManager', RESEED, 0)


	select * from Assignment
	select * from TaskStatus
	select * from TaskLog
	select * from task
	select * from employee
	select * from comment
	use ToDoList 

	select * from task
	select * from Assignment

	select e.Name, a.TaskId
	from Assignment as a 
	join Employee as e
	on a.EmployeeId = e.Id
	where TaskId = 7